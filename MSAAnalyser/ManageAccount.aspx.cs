﻿//Required Imports
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using AESEncDec;
using System.Web.UI.HtmlControls;

//Page Class - Holds all Events and Methods
public partial class ManageAccount : System.Web.UI.Page
{
    //Install Variable Selected user
    string selectedUser;

    protected void Page_Load(object sender, EventArgs e)
    {
        //Page Load Event
        try
        {
            //Checks Weather a Session Exists by checking UserName
            if (Session["UserName"] != null)
            {

                //Checks weather information has been sent from the client to the web server
                if (!Page.IsPostBack)
                {
                    //Auto Disaposable Method
                    using (MSAAnalyserEntities context = new MSAAnalyserEntities())
                    {
                        //Store Session Object to String
                        string username = Session["UserName"].ToString();

                        //Find and Select Username that is currently logged in
                        User user = context.Users.FirstOrDefault(r => r.UserName == username);

                        //Reset Drop Down Users list
                        DropDownUsers.Items.Clear();

                        //Attempt to Check if User is Admin
                        if (user.isAdmin == true)
                        {
                            //Title Change
                            accDescription.Text = "Manage Accounts | ";

                            //Select all users from Database and Put into List and then DropDown List
                            List<User> allUsers = context.Users.ToList();
                            allUsers.ForEach(r => DropDownUsers.Items.Add(r.UserName));

                            //Disable all Fields until item is selected
                            TBFirstName.Enabled = false;
                            TBLastName.Enabled = false;
                            TBUserName.Enabled = false;
                            TBPassWord.Enabled = false;
                            isAdminCB.Enabled = false;
                            BTNUpdate.CssClass = "btn-warning";
                            BTNUpdate.Enabled = false;
                            BTNDelete.CssClass = "btn-warning";
                            BTNDelete.Enabled = false;

                        }
                        else if (user.isAdmin == false)
                        {
                            //If User is not Admin - Change some Controls
                            accDescription.Text = "My Account";
                            DropDownUsers.Enabled = false;
                            isAdminCB.Enabled = false;
                            DropDownUsers.Items.Add(username);
                            BTNUpdate.CssClass = "btn-success";
                            BTNUpdate.Enabled = true;
                            BTNDelete.Visible = false;
                            BTNSelect.Visible = false;
                            btnShowModal.Visible = false;

                            //Load information for current user only
                            TBFirstName.Text = user.FirstName;
                            TBLastName.Text = user.LastName;
                            TBUserName.Text = user.UserName;
                            TBPassWord.Text = AESCrypt.Decrypt(user.PassWord);
                            isAdminCB.Checked = user.isAdmin;

                        }
                    }
                }
            }
            else
            {
                //Redirect user to Login Page
                Response.Redirect("/Default.aspx");
            }

        }
        catch (NullReferenceException)
        {
            //Redirect user to Login Page
            Response.Redirect("/Default.aspx");
        }
    }

    protected void logOutButton_Click(object sender, EventArgs e)
    {
        //Kill Session
        Session.Abandon();

        //Redirect user to Login Page
        Response.Redirect("/Default.aspx");

    }

    //When the Select User Button is pressed
    protected void BTNSelect_Click(object sender, EventArgs e)
    {
        //Auto Disaposable Method
        using (MSAAnalyserEntities context = new MSAAnalyserEntities())
        {
            //Store Selected User in String
            selectedUser = DropDownUsers.SelectedItem.Text;

            //Attempt to find this User using ADO.NET
            User user = context.Users.FirstOrDefault(u => u.UserName == selectedUser);

            //Change Controls
            TBFirstName.Enabled = true;
            TBLastName.Enabled = true;
            TBUserName.Enabled = true;
            TBPassWord.Enabled = true;
            isAdminCB.Enabled = true;
            BTNUpdate.CssClass = "btn-success";
            BTNUpdate.Enabled = true;
            BTNDelete.CssClass = "btn-success";
            BTNDelete.Enabled = true;
            FieldValidator.Visible = false;

            //Load information for Selected user only -- Show Decrypted Password
            TBFirstName.Text = user.FirstName;
            TBLastName.Text = user.LastName;
            TBUserName.Text = user.UserName;
            TBPassWord.Text = AESCrypt.Decrypt(user.PassWord);
            isAdminCB.Checked = user.isAdmin;

        }

    }

    //When Delete Button is Pressed
    protected void BTNDelete_Click(object sender, EventArgs e)
    {
        //Auto Disaposable Method
        using (MSAAnalyserEntities context = new MSAAnalyserEntities())
        {
            //Store Selected User in String
            selectedUser = DropDownUsers.SelectedItem.Text;

            //Attempt to find this User using ADO.NET
            User user = context.Users.FirstOrDefault(u => u.UserName == selectedUser);

            //We Check if the Current Admin Logged in Attempts to Delete their Own Account
            if (Session["UserName"].ToString() == user.UserName)
            {
                //Display Error
                FieldValidator.CssClass = "alert-danger";
                FieldValidator.Visible = true;
                FieldValidator.Text = "You cannot delete yourself!";
            }
            else
            {
                //Remove User from Database and Commit Changes
                context.Users.Remove(user);
                context.SaveChanges();

                //Display Success Message
                FieldValidator.CssClass = "alert-success";
                FieldValidator.Visible = true;
                FieldValidator.Text = selectedUser + " has successfully been deleted! This page will now be refreshed.";

                //Refresh Page in 2.5 Seconds
                HtmlMeta meta = new HtmlMeta();
                meta.HttpEquiv = "Refresh";
                meta.Content = "2.5;url=ManageAccount.aspx";
                this.Page.Controls.Add(meta);
            }

        }
    }

    //When the Add Button is Pressed
    protected void BTNAdd_Click(object sender, EventArgs e)
    {
        //Auto Disaposable Method
        using (MSAAnalyserEntities context = new MSAAnalyserEntities())
        {
            //Create new Entity User
            User user = new User();

            //Check if Any of the Fields are blank - Display Error
            if (FirstNameTB.Text == "" || LastNameTB.Text == "" || UserNameTB.Text == "" || PassWordTB.Text == "")
            {
                AddFormValidate.CssClass = "alert-danger";
                AddFormValidate.Visible = true;
                AddFormValidate.Text = "All fields are required to be entered!";

            }
            else
            {
                //Check if a Username already Exists in the Same Database - Display Error
                if (context.Users.Any(o => o.UserName == UserNameTB.Text))
                {
                    AddFormValidate.CssClass = "alert-danger";
                    AddFormValidate.Visible = true;
                    AddFormValidate.Text = "Username already exists. Please enter a different user name.";
                    UserNameTB.Text = "";

                }
                else
                {
                    //If All is well, Add Data to the Appropriate Entity Object and DB
                    user.FirstName = FirstNameTB.Text;
                    user.LastName = LastNameTB.Text;
                    user.UserName = UserNameTB.Text;
                    user.PassWord = AESCrypt.Encrypt(PassWordTB.Text);
                    user.isAdmin = newIsAdminCB.Checked;

                    //Add User to ADO and Commit Changes
                    context.Users.Add(user);
                    context.SaveChanges();

                    //Display Success Message 
                    AddFormValidate.CssClass = "alert-success";
                    AddFormValidate.Visible = true;
                    AddFormValidate.Text = user.UserName + " has successfully been added! This page will now be refreshed.";

                    //Refresh Page in 2 Seconds
                    HtmlMeta meta = new HtmlMeta();
                    meta.HttpEquiv = "Refresh";
                    meta.Content = "2;url=ManageAccount.aspx";
                    this.Page.Controls.Add(meta);
                }

            }

        }

        //Keep Model Open After it is Been Clicked
        ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "showModal();", true);
    }

    //When the Update Button is Pressed
    protected void BTNUpdate_Click(object sender, EventArgs e)
    {
        //Auto Dispoable Method
        using (MSAAnalyserEntities context = new MSAAnalyserEntities())
        {
            //Store Selected User in String
            selectedUser = DropDownUsers.SelectedItem.Text;

            //Attempt to find this User using ADO.NET
            User user = context.Users.FirstOrDefault(u => u.UserName == selectedUser);

            //Check if Any of the Fields 
            if (TBFirstName.Text == "" || TBLastName.Text == "" || TBUserName.Text == "" || TBPassWord.Text == "")
            {
                FieldValidator.CssClass = "alert-danger";
                FieldValidator.Visible = true;
                FieldValidator.Text = "All fields are required to be entered!";

            }
            else
            {
                //Check is username field has changed
                if (TBUserName.Text != selectedUser)
                {
                    //Check if User Already Exists in Database - Display Error
                    if (context.Users.Any(o => o.UserName == TBUserName.Text))
                    {
                        FieldValidator.CssClass = "alert-danger";
                        FieldValidator.Visible = true;
                        FieldValidator.Text = "Username already exists. Please enter a different user name.";
                        TBUserName.Text = "";

                    }
                    else
                    {
                        //Store Objects in Entity Objects
                        user.FirstName = TBFirstName.Text;
                        user.LastName = TBLastName.Text;
                        user.UserName = TBUserName.Text;
                        user.PassWord = AESCrypt.Encrypt(TBPassWord.Text);
                        user.isAdmin = isAdminCB.Checked;

                        //Commit and Save Changes to Database
                        context.SaveChanges();

                        //Display Success Message
                        FieldValidator.CssClass = "alert-success";
                        FieldValidator.Visible = true;
                        FieldValidator.Text = selectedUser + " has successfully been updated! This page will now be refreshed.";

                        //Refresh Page in 2.5 Seconds
                        HtmlMeta meta = new HtmlMeta();
                        meta.HttpEquiv = "Refresh";
                        meta.Content = "2.5;url=ManageAccount.aspx";
                        this.Page.Controls.Add(meta);
                    }

                }
                else
                {
                    //Store Objects in Entity Objects
                    user.FirstName = TBFirstName.Text;
                    user.LastName = TBLastName.Text;
                    user.UserName = TBUserName.Text;
                    user.PassWord = AESCrypt.Encrypt(TBPassWord.Text);
                    user.isAdmin = isAdminCB.Checked;

                    //Commit and Save Changes to Database
                    context.SaveChanges();

                    //Update Session Data
                    Session["UserName"] = TBUserName.Text;

                    //Display Success Message
                    FieldValidator.CssClass = "alert-success";
                    FieldValidator.Visible = true;
                    FieldValidator.Text = selectedUser + " has successfully been updated! This page will now be refreshed.";

                    //Refresh Page in 2.5 Seconds
                    HtmlMeta meta = new HtmlMeta();
                    meta.HttpEquiv = "Refresh";
                    meta.Content = "2.5;url=ManageAccount.aspx";
                    this.Page.Controls.Add(meta);
                }

            }

        }
    }

    //If the Drop Down Users Item has Changed
    protected void DropDownUsers_SelectedIndexChanged(object sender, EventArgs e)
    {
        //get current value and compare with new value then disable button if the same
        if (DropDownUsers.SelectedValue != selectedUser)
        {
            //Disable Cetain Features until Select is Pressed
            BTNUpdate.CssClass = "btn-warning";
            BTNUpdate.Enabled = false;

            BTNDelete.CssClass = "btn-warning";
            BTNDelete.Enabled = false;

        }
        else
        {
            //Enable Features if not changed
            BTNUpdate.CssClass = "btn-success";
            BTNUpdate.Enabled = true;

            BTNDelete.CssClass = "btn-success";
            BTNDelete.Enabled = true;
        }


    }

}