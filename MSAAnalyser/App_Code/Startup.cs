﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(MSAAnalyser.Startup))]
namespace MSAAnalyser
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
