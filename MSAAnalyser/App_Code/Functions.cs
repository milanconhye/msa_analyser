﻿//Required Imports
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

//Package Name
namespace Function
{
    //Class Name
    public class Functions
    {
        //Select All Organisations From Database in a Specific Record Year
        public static void UpdateUniversityDataField(DropDownList DropDownListUniversity, DropDownList DropDownRecordYear)
        {
            //Auto Disposable Method
            using (MSAAnalyserEntities context = new MSAAnalyserEntities())
            {
                DropDownListUniversity.Items.Clear();
                var universityList = context.TableRecords.Where(recordType => recordType.RecordYear == DropDownRecordYear.SelectedValue).Select(r => r.OrganisationName).ToList();
                universityList.ForEach(r => DropDownListUniversity.Items.Add(r));
            }

        }

        //Load Dates From Database and Place in DropDownRecord Year
        public static void LoadDates(DropDownList DropDownRecordYear)
        {
            //Auto Disposable Method
            using (MSAAnalyserEntities entities = new MSAAnalyserEntities())
            {
                var allDates = entities.TableDates.Select(r => r.Date).ToList();
                DropDownRecordYear.DataSource = allDates;
                DropDownRecordYear.DataBind();

                DropDownRecordYear.Items.Insert(0, "Select Value");

            }
        }

        //Load All Criterias and Store in CheckBoxCriteria
        public static void LoadCriteriaList(CheckBoxList CheckBoxCriteria)
        {
            //Auto Disposable Method
            using (MSAAnalyserEntities entities = new MSAAnalyserEntities())
            {
                var allCriterias = entities.TableCriterias.Select(r => r.CriteriaName).ToList();
                CheckBoxCriteria.DataSource = allCriterias;
                CheckBoxCriteria.DataBind();

            }
        }

        //Load Criteria Menu using Organisation Names 
        public static void LoadCriteriaMenu(string organisationName, Repeater rptCriteria)
        {
            //Auto Disposable Method
            using (MSAAnalyserEntities entities = new MSAAnalyserEntities())
            {
                //Select From Table Records
                TableRecord tableRecords = entities.TableRecords.FirstOrDefault(tr => tr.OrganisationName == organisationName);

                //Select From Table Record Criteria
                TableRecordCriteria tableRecordCriteria = entities.TableRecordCriterias.FirstOrDefault(trc => trc.RecordId == tableRecords.Id);

                //Store All TableCriteria Records in List
                List<TableCriteria> allTableCriteriaRecords = entities.TableCriterias.ToList();

                //Installise List to Store All Criterias
                List<string> allCriterias = new List<string>();

                //We now have list of All Criterias
                allTableCriteriaRecords.ForEach(r => allCriterias.Add(r.CriteriaName));

                //Install Table Criterias
                TableCriteria tableCriteria;

                //Installise List to Store All Records with specfic Organisation Name
                List<string> records = new List<string>();

                //For each item in the List Store in Records List
                foreach (var item in allCriterias)
                {
                    //Find Criteria from each Item
                    tableCriteria = entities.TableCriterias.FirstOrDefault(tc => tc.CriteriaName == item.ToString());

                    //Select Records where The Criteria Id and Record Criteria Matches
                    var selectRecord = entities.TableRecordCriterias.Where(recordType => recordType.CriteriaId == tableCriteria.Id && recordType.RecordId == tableRecords.Id).FirstOrDefault();

                    //If it does not equal to Null
                    if (selectRecord != null)
                    {
                        //If the Reported Citeria is True...
                        if (selectRecord.CriteriaReported == true)
                        {
                            //Add Records to List
                            records.Add(selectRecord.TableCriteria.CriteriaName.ToString());
                        }
                    }

                }

                //Add List to Repeater and Bind Data
                rptCriteria.DataSource = records;
                rptCriteria.DataBind();

            }
        }

        //Load SubCriteria List from a Specfic Criteria Selected and Place Data into a CheckBox List
        public static void LoadSubCriteriaList(TableCriteria tableCriteria, CheckBoxList CheckBoxSubCriteria)
        {
            //Auto Disposable Method
            using (MSAAnalyserEntities entities = new MSAAnalyserEntities())
            {
                List<TableSubCriteria> allCriterias = tableCriteria.TableSubCriterias.ToList();
                CheckBoxSubCriteria.DataSource = allCriterias.Select(r => r.SubCriteriaName).ToList();
                CheckBoxSubCriteria.DataBind();

            }
        }

      

    }
}
