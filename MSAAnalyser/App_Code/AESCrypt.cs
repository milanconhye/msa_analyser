﻿//Required Imports
using System;
using System.Security.Cryptography;
using System.Text;

namespace AESEncDec
{
   /*
    * AES CBS 256-bit Encryption using .NET Library
    * Used to Encrypt and Decrypt Private Values on the Database for Reterival 
    */

    public class AESCrypt
    {
       
        //Initialization vector for Random Semantic Security
        public static string IV = "f81kdl3m9zj2jdna";

        //Encryption and Decryption Key
        public static string Key = "7924423F4528482B4D6251655468576D";

        //Encryption method takes @param stringbase64
        public static string Encrypt(string decrypted)
        {
            //Converting the Text to a Byte Array
            byte[] textbytes = ASCIIEncoding.ASCII.GetBytes(decrypted);

            //.NET AES Library 
            AesCryptoServiceProvider encdec = new AesCryptoServiceProvider();

            //Instantiating Encryption Values
            encdec.BlockSize = 128;
            encdec.KeySize = 256;
            encdec.Key = ASCIIEncoding.ASCII.GetBytes(Key);
            encdec.IV = ASCIIEncoding.ASCII.GetBytes(IV);
            encdec.Padding = PaddingMode.PKCS7;
            encdec.Mode = CipherMode.CBC;

            //Creating Encryption with Key and IV
            ICryptoTransform icrypt = encdec.CreateEncryptor(encdec.Key, encdec.IV);

            //Converting Encryption with Key and IV to Byte Array
            byte[] enc = icrypt.TransformFinalBlock(textbytes, 0, textbytes.Length);

            //Releasing Resources as there no longer needed
            icrypt.Dispose();

            //Converting the byte array to a string so it can be stored on to the DataBase
            return Convert.ToBase64String(enc);
        }

        //Decryption method takes @param stringbase64
        public static string Decrypt(string encrypted)
        {
            //Convert Encrypted Text to Byte Array
            byte[] encbytes = Convert.FromBase64String(encrypted);

            //.NET AES Library
            AesCryptoServiceProvider encdec = new AesCryptoServiceProvider();

            //Instantiating Decryption Values
            encdec.BlockSize = 128;
            encdec.KeySize = 256;
            encdec.Key = ASCIIEncoding.ASCII.GetBytes(Key);
            encdec.IV = ASCIIEncoding.ASCII.GetBytes(IV);
            encdec.Padding = PaddingMode.PKCS7;
            encdec.Mode = CipherMode.CBC;
            
            //Creating Encryption with Key and IV
            ICryptoTransform icrypt = encdec.CreateDecryptor(encdec.Key, encdec.IV);

            //Converting Encryption with Key and IV to Byte Array
            byte[] dec = icrypt.TransformFinalBlock(encbytes, 0, encbytes.Length);

            //Releasing Resources as there no longer needed
            icrypt.Dispose();

            //Converting the byte array to a string so it can be stored on to the DataBase
            return ASCIIEncoding.ASCII.GetString(dec);
        }

    }
}