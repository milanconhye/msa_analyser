﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>DashBoard - MSA Analyser</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <link rel="stylesheet" href="Content/Site.css" />
    <link rel="stylesheet" href="Content/bootstrap.css" />

    <script src="Scripts/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.button.js" type="text/javascript"></script>
    <script src="js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="js/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.ui.combobox.js" type="text/javascript"></script>

    <style type="text/css">
        .auto-style25 {
            color: inherit;
            font-weight: 200;
            line-height: 2.1428571435;
            text-align: left;
            margin-left: 40px;
            margin-right: 40px;
            margin-top: 0px;
            margin-bottom: 30px;
            padding: 38px 20px;
            background-color: #eeeeee;
            width: 95%;
        }

        table {
            width: 450px;
            margin-bottom: -1px;
        }

            table th {
                background-color: #F7F7F7;
                color: #333;
                font-weight: bold;
            }

            table th, table td {
                padding: 5px;
                border-color: #ccc;
            }
    </style>

</head>
<body>
    <form id="form1" runat="server">

        <div class="auto-style25">

            <table style="width: 100%;">
                <tr>
                    <td>
                        <asp:Label ID="FieldValidator" runat="server" CssClass="alert-danger" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>

            <h1>Login</h1>
            <p class="lead">Login to view and manage the Modern Slavery Act Databases</p>

            <table style="width: 100%;">
                <tr>
                    <td style="width: 113px">
                        <asp:Label ID="LabelUsername" runat="server" Text="Username:"></asp:Label>
                    </td>
                    <td style="width: 173px">
                        <asp:TextBox ID="TBUsername" runat="server" Height="27px" Width="170px"></asp:TextBox>
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 113px; height: 32px;">
                        <asp:Label ID="LabelPassword" runat="server" Text="Password:"></asp:Label>
                    </td>
                    <td style="height: 32px; width: 173px">
                        <asp:TextBox ID="TBPassword" TextMode="Password" runat="server" Height="27px" Width="170px"></asp:TextBox>
                    </td>
                    <td style="height: 32px">&nbsp;</td>
                    <td style="height: 32px"></td>
                </tr>
                <tr>
                    <td style="width: 113px">&nbsp;</td>
                    <td class="text-right" style="width: 173px">
                        <asp:Button ID="BTNLogin" runat="server" Height="32px" Style="font-size: small" Text="Login" Width="69px" CssClass="btn-success active" EnableViewState="False" OnClick="BTNLogin_Click" />
                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <hr />
        <footer style="margin-left: 40px;">
        <p>Copyright &copy; <%: DateTime.Now.Year %> MSA Analyser | University of Greenwich | Developed by <a href="http://www.milanconhye.com" target="_blank">Milan Conhye</a> &amp; <a href="https://www.gre.ac.uk/ach/study/cis/gwizards/home" target="_blank">GWIZARDS</a></p>
    <span id="siteseal"><script async="async" type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=v1vRkL5x8BW6FacK2b2Yv6eMP7l59wtHyG1BFhpzgial5cD3287aGd5eVg8r"></script></span>
         <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async="async"></script>
        <img src="mcafee.png" alt="MCAFEE" style="width:8%; height:60%" />
        </footer>
    </form>
</body>

</html>
