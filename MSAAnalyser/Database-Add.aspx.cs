﻿//Required Imports
using System;
using System.Linq;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using Function;

//Page Class - Holds all Events and Methods
public partial class DatabaseAdd : System.Web.UI.Page
{
    //Page Load Event
    protected void Page_Load(object sender, EventArgs e)
    {
        //If Its not a PostBack (Page Refresh)
        if (!this.IsPostBack)
        {

            //Checks Weather a Session Exists by checking UserName
            if (Session["UserName"] != null)
            {
                //Auto Disaposable Method
                using (MSAAnalyserEntities context = new MSAAnalyserEntities())
                {
                    //Store Session Object to String
                    string username = Session["UserName"].ToString();

                    //Find and Select Username that is currently logged in
                    User user = context.Users.FirstOrDefault(r => r.UserName == username);

                    //Check If user is an Admin or not
                    if (!user.isAdmin)
                    {
                        //If not then hide Delete Record Button
                        BtnDeleteRecord.Visible = false;
                    }

                }

                //Load Dates from Database and Store in DropDownList
                Functions.LoadDates(DropDownRecordYear);

                //Load All Criterias and Print them about in CheckBox List
                Functions.LoadCriteriaList(CheckBoxCriteria);

            }
            else
            {
                //Redirect User to Login Page
                Response.Redirect("/Default.aspx");
            }
        }
    }

    //Bind CheckBox Click and Hide Div Tag if False, show if true.
    protected string DefinirVisibilidad()
    {
        return this.chkOther.Checked == true ? "display:block" : "display:none";
    }

    protected void logOutButton_Click(object sender, EventArgs e)
    {
        //Kill Session
        Session.Abandon();

        //Redirect user to Login Page
        Response.Redirect("/Default.aspx");
    }

    protected void btnAdd_Click(object sender, EventArgs e)
    {
        //Auto Disaposable Method
        using (MSAAnalyserEntities context = new MSAAnalyserEntities())
        {
            //Installise New Table Records for Adding
            TableRecord tableRecords = new TableRecord();

            //Check if any Sector CheckBoxes has been Selected
            bool hasAnyServicesBeenSelected = CBServicesList.Items.Cast<ListItem>().Any(i => i.Selected);

            //Check if any Criteria CheckBoxes has been Selected
            bool hasAnyCriteriasBeenSelected = CheckBoxCriteria.Items.Cast<ListItem>().Any(i => i.Selected);

            //Check if University Already Exists in Database thats on the Same Date
            var checkIfRecordExists = context.TableRecords.Where(r => r.RecordYear == DropDownRecordYear.SelectedItem.Text).ToList();

            //Check if any of the Required fields are blank
            if (txtUniversityName.Text == "" && !hasAnyServicesBeenSelected && !hasAnyCriteriasBeenSelected && DropDownRecordYear.SelectedIndex == 0)
            {
                FieldValidator.CssClass = "alert-danger";
                FieldValidator.Visible = true;
                FieldValidator.Text = "University, Sector, Criteria and Record Year are all Required Fields!";

            }

            //Check if the University Name is Blank
            else if (txtUniversityName.Text == "")
            {
                FieldValidator.CssClass = "alert-danger";
                FieldValidator.Visible = true;
                FieldValidator.Text = "University Name cannot be blank!";

            }

            //Checking if any of the Criteria CheckBoxes has been selected - NOT
            else if (!hasAnyCriteriasBeenSelected)
            {
                FieldValidator.CssClass = "alert-danger";
                FieldValidator.Visible = true;
                FieldValidator.Text = "You are required to at least select one Criteria!";

            }


            //Checking if any of the Sector CheckBoxes has been selected - NOT
            else if (!hasAnyServicesBeenSelected)
            {
                FieldValidator.CssClass = "alert-danger";
                FieldValidator.Visible = true;
                FieldValidator.Text = "You are required to at least select one Sector!";

            }

            //Check if the Record Year is still on its Default Value
            else if (DropDownRecordYear.SelectedItem.Text == "Select Value")
            {
                FieldValidator.CssClass = "alert-danger";
                FieldValidator.Visible = true;
                FieldValidator.Text = "You must choose a Record Year!";

            }

            //Checing if Any Record Exists under the same Record Year
            else if (checkIfRecordExists.Any(item => item.OrganisationName == txtUniversityName.Text))
            {
                FieldValidator.CssClass = "alert-danger";
                FieldValidator.Visible = true;
                FieldValidator.Text = "Organisation Name Already Exists under the same Record Year";

            } else
            {
                //If all is good then store the Required Fields into the Entity Objects
                tableRecords.OrganisationName = txtUniversityName.Text;
                tableRecords.HQ = DropDownHQ.SelectedValue;

                //Finding the Sector CheckBoxes
                CheckBoxList ServicesCheckBox = (CheckBoxList)this.FindControl("CBServicesList");

                //Seeing if the Count is Greater then 0
                if (ServicesCheckBox.Items.Count > 0)
                {
                    //For each item thats selected, convert to List Item and Spilt with Comma and Store in Object
                    foreach (ListItem item in ServicesCheckBox.Items)
                    {
                        //If Item is checked
                        if (item.Selected)
                        {
                            tableRecords.Sector += item.Value + ",";
                        }
                    }
                }

                //If the Other CheckBox has been Checked, if true store in Object
                if (chkOther.Checked == true)
                {
                    tableRecords.SectorOther = txtOther.Value;
                }

                //If the checked Items are Greater then 0
                if (CBCollabList.Items.Count > 0)
                {
                    //For each Item that is selected Spilt with Comma and Store in Object
                    foreach (ListItem item in CBCollabList.Items)
                    {
                        if (item.Selected)
                        {
                            tableRecords.Collaboration += item.Value + ",";
                        }
                    }
                }

                //Store Required Fields into Entity Objects
                tableRecords.SuspectedTemplate = DropDownSuspect.SelectedValue;
                tableRecords.SizeOfStatement = DropDownSizeOfStatement.Text;
                tableRecords.TurnOver = txtTurnOver.Text;
                tableRecords.URL = txtURL.Text;
                tableRecords.Signed = cbSigned.Checked;
                tableRecords.SignedOther = DropDownSignedByOther.SelectedValue;
                tableRecords.HomePage = cbHomePage.Checked;
                tableRecords.HomePageOther = DropDownHomePageOther.SelectedValue;
                tableRecords.Introduction = cbIntroduction.Checked;
                tableRecords.Commentary = txtCommentary.Value;
                tableRecords.RecordYear = DropDownRecordYear.SelectedValue;

                //Add Object Model to Database and Commit Changes
                context.TableRecords.Add(tableRecords);
                context.SaveChanges();

                //Installise new Table Record Criteria
                TableRecordCriteria tableRecordCriteria = new TableRecordCriteria();

                //Select Data from Database using the Organisation Name
                tableRecords = context.TableRecords.FirstOrDefault(t => t.OrganisationName == txtUniversityName.Text);

                //Install Table Criteria
                TableCriteria tableCriteria;

                //Find CheckBoxCriteria Control
                CheckBoxList CriteriaCheckBox = (CheckBoxList)this.FindControl("CheckBoxCriteria");

                //Check if the Selcted item count is greater then 0
                if (CriteriaCheckBox.Items.Count > 0)
                {
                    //For each item that is selected from the Criteria CheckBox Items
                    foreach (ListItem item in CriteriaCheckBox.Items)
                    {
                        //If the Item is Selected
                        if (item.Selected)
                        {
                            //Find and Select Each Value Corrosponding to the Item Value
                            tableCriteria = context.TableCriterias.FirstOrDefault(r => r.CriteriaName == item.Value);

                            //Attempt to Try Placing Records in appropriate Objects
                            try
                            {
                                //Parse Fields into Objects to Fill Database
                                tableRecordCriteria.RecordId = tableRecords.Id;
                                tableRecordCriteria.CriteriaId = tableCriteria.Id;
                                tableRecordCriteria.CriteriaReported = item.Selected;
                                item.Selected = tableRecordCriteria.CriteriaReported;

                                //Add Object Model to Database and Commit Changes
                                context.TableRecordCriterias.Add(tableRecordCriteria);
                                context.SaveChanges();

                                //Display Success Message to User
                                FieldValidator.CssClass = "alert-success";
                                FieldValidator.Visible = true;
                                FieldValidator.Text = txtUniversityName.Text + " has successfully been added!";

                                //Refresh Page in 2.5 Seconds
                                HtmlMeta meta = new HtmlMeta();
                                meta.HttpEquiv = "Refresh";
                                meta.Content = "2.5;url=Database-Add.aspx";
                                this.Page.Controls.Add(meta);

                            }
                            catch (Exception ex)
                            {
                                //Throw Exception if anything was to go wrong
                                Console.WriteLine(ex);
                            }
                        }

                    }

                }

            }

        }
    }

    //If user has presed Cancel while editing, Refresh Page
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(Request.RawUrl);
    }
}