﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ByOrganisation.aspx.cs" Inherits="ByOrganisation" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>View Organisations - MSA Analyser</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <link rel="stylesheet" href="Content/Site.css" />
    <link rel="stylesheet" href="Content/bootstrap.css" />

    <script src="Scripts/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.button.js" type="text/javascript"></script>
    <script src="js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="js/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.ui.combobox.js" type="text/javascript"></script>

    <style type="text/css">
        .auto-style25 {
            color: inherit;
            font-weight: 200;
            line-height: 2.1428571435;
            text-align: left;
            margin-left: 40px;
            margin-right: 40px;
            margin-top: 0px;
            margin-bottom: 30px;
            padding: 38px 20px;
            background-color: #eeeeee;
            width: 95%;
        }

        body {
            font-family: Arial;
            font-size: 18px;
        }

        table {
            width: 450px;
            margin-bottom: -1px;
        }

            table th {
                background-color: #F7F7F7;
                color: #333;
                font-weight: bold;
            }

            table th, table td {
                padding: 5px;
                border-color: #ccc;
            }

        .hidden {
            display: none;
        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #f3f3f3;
        }

        li {
            float: left;
        }

            li a {
                display: block;
                color: black;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

                li a:hover:not(.active) {
                    background-color: #1d8cba;
                    text-decoration: none;
                    color: white;
                }

                li a:hover {
                    text-decoration: none;
                    color: white;
                }

        .active {
            background-color: #1d8cba;
            color: white;
        }

        a {
           text-decoration: none;
        }

    </style>

    <script type="text/javascript">
        function ClearSession() {
            document.getElementById("ButtonLogOut").click();
        }


    </script>

</head>
<body>

    <form id="form1" runat="server">

        <div class="auto-style25">

            <div style="width: auto; height: auto; font-size: 14px;">
            <ul>
                <li><a href="Dashboard.aspx" class="active">DashBoard</a></li>
                <li><a href="ManageAccount.aspx">Manage Accounts</a></li>
                <li><a href="#" onclick="ClearSession()">Sign Out</a></li>
            </ul>
            <asp:Button ID="ButtonLogOut" runat="server" CausesValidation="False" OnClick="logOutButton_Click" CssClass="hidden"></asp:Button>
            </div>

            <br />
            <asp:GridView runat="server" ID="AllOrganisations" AutoGenerateColumns="false" Font-Bold="False" Font-Size="Small">
                <Columns>
                    <asp:TemplateField HeaderText="Organisation Name">
                        <ItemTemplate><%#Eval("OrganisationName")%></ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="HQ">
                        <ItemTemplate><%#Eval("HQ")%></ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Sector">
                        <ItemTemplate><%#Eval("Sector")%></ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Size of Statement">
                        <ItemTemplate><%#Eval("SizeOfStatement")%></ItemTemplate>
                    </asp:TemplateField>  

                     <asp:TemplateField HeaderText="Turn Over">
                        <ItemTemplate><%#Eval("TurnOver")%></ItemTemplate>
                    </asp:TemplateField> 
                    
                     <asp:TemplateField HeaderText="URL">
                        <ItemTemplate><%#Eval("URL")%></ItemTemplate>
                    </asp:TemplateField>  
                    
                    <asp:TemplateField HeaderText="Signed">
                        <ItemTemplate><asp:CheckBox ID="chkSelect" runat="server" Checked='<%# Bind("Signed") %>' /></ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Signed Other">
                        <ItemTemplate><%#Eval("SignedOther")%></ItemTemplate>
                    </asp:TemplateField> 
                    
                    <asp:TemplateField HeaderText="Home Page">
                         <ItemTemplate><asp:CheckBox ID="chkSelectHP" runat="server" Checked='<%# Bind("HomePage") %>' /></ItemTemplate>
                    </asp:TemplateField>
                    
                    <asp:TemplateField HeaderText="Home Page Other">
                        <ItemTemplate><%#Eval("HomePageOther")%></ItemTemplate>
                    </asp:TemplateField>    
                    
                    <asp:TemplateField HeaderText="Introduction">
                       <ItemTemplate><asp:CheckBox ID="chkSelectIntro" runat="server" Checked='<%# Bind("Introduction") %>' /></ItemTemplate>
                    </asp:TemplateField> 

                    <asp:TemplateField HeaderText="Commentary">
                        <ItemTemplate><%#Eval("Commentary")%></ItemTemplate>
                    </asp:TemplateField> 

                    <asp:TemplateField HeaderText="RecordYear">
                        <ItemTemplate><%#Eval("RecordYear")%></ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Sector Other">
                        <ItemTemplate><%#Eval("SectorOther")%></ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Collaboration">
                        <ItemTemplate><%#Eval("Collaboration")%></ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Suspected Template">
                        <ItemTemplate><%#Eval("SuspectedTemplate")%></ItemTemplate>
                    </asp:TemplateField>

                </Columns>  
            </asp:GridView> 
            <br />
        </div>
        <hr />
        <footer style="margin-left: 40px; font-size: 14px">
            <p>Copyright &copy; <%: DateTime.Now.Year %> MSA Analyser | University of Greenwich | Developed by <a href="http://www.milanconhye.com" target="_blank">Milan Conhye</a> &amp; <a href="https://www.gre.ac.uk/ach/study/cis/gwizards/home" target="_blank">GWIZARDS</a></p>
            <span id="siteseal"><script async="async" type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=v1vRkL5x8BW6FacK2b2Yv6eMP7l59wtHyG1BFhpzgial5cD3287aGd5eVg8r"></script></span>
         <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async="async"></script>
        <img src="mcafee.png" alt="MCAFEE" style="width:8%; height:60%" />
        </footer>
    </form>
</body>
</html>