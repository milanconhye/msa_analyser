﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeFile="ManageAccount.aspx.cs" Inherits="ManageAccount" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Manage Account - MSA Analyser Login</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <link rel="stylesheet" href="Content/Site.css" />
    <link rel="stylesheet" href="Content/bootstrap.css" />

    <script src="Scripts/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <style type="text/css">
        .auto-style3 {
            height: 44px;
        }

        .auto-style5 {
            width: 117px;
            height: 44px;
        }

        .auto-style8 {
            height: 44px;
            text-align: right;
            width: 178px;
        }

        .auto-style10 {
            height: 50px;
            width: 178px;
        }

        .auto-style12 {
            width: 45%;
        }

        .auto-style13 {
            width: 117px;
            height: 58px;
        }

        .auto-style14 {
            height: 58px;
            width: 178px;
        }

        .auto-style15 {
            height: 58px;
        }

        .auto-style16 {
            width: 117px;
            height: 54px;
        }

        .auto-style17 {
            height: 54px;
            width: 178px;
        }

        .auto-style18 {
            height: 54px;
        }

        .auto-style21 {
            width: 117px;
            height: 50px;
        }

        .auto-style22 {
            height: 50px;
        }

        auto-style12 {
            padding-left: 20%;
        }

        .auto-style25 {
            color: inherit;
            font-weight: 200;
            line-height: 2.1428571435;
            text-align: left;
            margin-left: 40px;
            margin-right: 40px;
            margin-top: 0px;
            margin-bottom: 30px;
            padding: 38px 20px;
            background-color: #eeeeee;
            width: 95%;
        }

        body {
            font-family: Arial;
            font-size: 14px;
        }

        .auto-style26 {
            font-size: medium;
        }

        .FieldVald {
            font-size: large;
            color: #CC0000;
        }

        .hidden {
            display: none;
        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #f3f3f3;
        }

        li {
            float: left;
        }

            li a {
                display: block;
                color: black;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

                li a:hover:not(.active) {
                    background-color: #1d8cba;
                    text-decoration: none;
                    color: white;
                }

                li a:hover {
                    text-decoration: none;
                    color: white;
                }

        .active {
            background-color: #1d8cba;
            color: white;
        }

        a {
            text-decoration: none;
        }
        .auto-style27 {
            font-size: 14px;
        }
    </style>



    <script type="text/javascript">
        function showModal() {
            $('#myModal').modal('show');
        }

        $(function () {
            $("#btnShowModal").click(function () {
                showModal();
            });
        });

        function ClearSession() {
            document.getElementById("ButtonLogOut").click();
        }
    </script>

</head>
<body>

    <form id="formManage" runat="server">

        <div class="auto-style25">

            <div style="width: auto; height: auto; font-size: 14px;">
                <ul>
                    <li><a href="Dashboard.aspx">DashBoard</a></li>
                    <li><a href="ManageAccount.aspx" class="active">Manage Accounts</a></li>
                    <li><a href="#" onclick="ClearSession()">Sign Out</a></li>
                </ul>
                <asp:Button ID="ButtonLogOut" runat="server" CausesValidation="False" OnClick="logOutButton_Click" CssClass="hidden"></asp:Button>
            </div>

            <br />
            <asp:Label ID="FieldValidator" runat="server" CssClass="alert-danger" Visible="False"></asp:Label>


            &nbsp;<br />

            <asp:Label ID="accDescription" runat="server" Text="Manage Accounts" Height="28px" Font-Size="24px"></asp:Label>


            &nbsp;<input type="button" id="btnShowModal" value="Add User" runat="server" class="btn-primary active" style="height: 26px; width: 95px; font-size: 12px" />
            <br />
            <br />

            <div id="myModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <div class="form-group">
                                <label>Add User</label>
                            </div>
                            <div class="form-group">
                                <asp:Label ID="AddFormValidate" runat="server" CssClass="alert-danger" Visible="False"></asp:Label>
                            </div>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label>First Name: </label>
                                <asp:TextBox ID="FirstNameTB" runat="server" Height="26px" Width="173px" CssClass="auto-style26"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Last Name: </label>
                                <asp:TextBox ID="LastNameTB" runat="server" Height="26px" Width="173px" CssClass="auto-style26"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Username: </label>
                                <asp:TextBox ID="UserNameTB" runat="server" Height="26px" Width="173px" CssClass="auto-style26"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>Password: </label>
                                <asp:TextBox ID="PassWordTB" runat="server" Height="26px" Width="173px" CssClass="auto-style26"></asp:TextBox>
                            </div>
                            <div class="form-group">
                                <label>IsAdmin: </label>
                                <asp:CheckBox ID="newIsAdminCB" runat="server" />
                            </div>
                        </div>
                        <div class="modal-footer">

                            <asp:Button ID="BTNAddUser" runat="server" OnClick="BTNAdd_Click" data-target="#login" data-toggle="modal" CssClass="btn-success" Text="Add User" Font-Size="Medium" Height="35px" Width="82px" />
                            <asp:Button ID="Close" runat="server" CssClass="btn-success" Text="Close" data-dismiss="modal" Font-Size="Medium" Height="35px" Width="82px" />
                        </div>
                    </div>
                </div>
            </div>


            <table class="auto-style12">

                <tr>
                    <td class="auto-style16">Select User:</td>
                    <td class="auto-style17">
                        <asp:DropDownList ID="DropDownUsers" runat="server" CssClass="auto-style2" Height="25px" Width="100px" Style="font-size: 14px" AutoPostBack="True" OnSelectedIndexChanged="DropDownUsers_SelectedIndexChanged"></asp:DropDownList>
                        &nbsp;<asp:Button ID="BTNSelect" runat="server" Text="Select" CssClass="btn-success active" OnClick="BTNSelect_Click" Height="25px" Style="font-size: 10px" />

                    </td>
                    <td class="auto-style18"></td>
                </tr>
                <tr>
                    <td class="auto-style16">First Name:</td>
                    <td class="auto-style17">
                        <asp:TextBox ID="TBFirstName" runat="server" Height="26px" Width="173px" CssClass="auto-style27"></asp:TextBox>
                    </td>
                    <td class="auto-style18"></td>
                </tr>
                <tr>
                    <td class="auto-style16">Last Name:</td>
                    <td class="auto-style17">
                        <asp:TextBox ID="TBLastName" runat="server" Height="26px" Width="173px" CssClass="auto-style27"></asp:TextBox>
                    </td>
                    <td class="auto-style18"></td>
                </tr>
                <tr>
                    <td class="auto-style16">Username:</td>
                    <td class="auto-style17">
                        <asp:TextBox ID="TBUserName" runat="server" Height="26px" Width="173px" CssClass="auto-style27"></asp:TextBox>
                    </td>
                    <td class="auto-style18"></td>
                </tr>
                <tr>
                    <td class="auto-style13">Password:</td>
                    <td class="auto-style14">
                        <asp:TextBox ID="TBPassWord" runat="server" ToolTip="Your password has been decrypted." Height="26px" Width="173px" CssClass="auto-style27"></asp:TextBox>
                    </td>
                    <td class="auto-style15"></td>
                </tr>
                <tr>
                    <td class="auto-style21">isAdmin:</td>
                    <td class="auto-style10">
                        <asp:CheckBox ID="isAdminCB" runat="server" />
                    </td>
                    <td class="auto-style22"></td>
                </tr>
                <tr>
                    <td class="auto-style5">&nbsp;</td>
                    <td class="auto-style8">
                        <asp:Button ID="BTNUpdate" runat="server" CssClass="btn-success active" Text="Update" Font-Size="Medium" Height="35px" Width="82px" OnClick="BTNUpdate_Click" Style="font-size: 11pt" />
                        &nbsp;<asp:Button ID="BTNDelete" runat="server" CssClass="btn-success active" Text="Delete" Font-Size="Medium" Height="35px" Width="82px" OnClick="BTNDelete_Click" OnClientClick="return confirm('Are you sure you want to delete this user?')" Style="font-size: 11pt" />

                    </td>
                    <td class="auto-style3">&nbsp;</td>
                </tr>
            </table>



        </div>


        <hr />
        <footer style="margin-left: 40px;">
            <p>Copyright &copy; <%: DateTime.Now.Year %> MSA Analyser | University of Greenwich | Developed by <a href="http://www.milanconhye.com" target="_blank">Milan Conhye</a> &amp; <a href="https://www.gre.ac.uk/ach/study/cis/gwizards/home" target="_blank">GWIZARDS</a></p>
            <span id="siteseal">
                <script async="async" type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=v1vRkL5x8BW6FacK2b2Yv6eMP7l59wtHyG1BFhpzgial5cD3287aGd5eVg8r"></script>
            </span>
            <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async="async"></script>
            <img src="mcafee.png" alt="MCAFEE" style="width: 8%; height: 60%" />
        </footer>

    </form>
</body>


</html>




