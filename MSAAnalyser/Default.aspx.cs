﻿//Required Imports
using System;
using System.Linq;
using System.Web.UI;
using AESEncDec;

//Page Class - Holds all Events and Methods
public partial class _Default : Page
{
    //Page Load Event
    protected void Page_Load(object sender, EventArgs e)
    {
        //if there is still a value within the Session, Redirect User to DashBoard
        if (Session["UserName"] != null)
        {
            Response.Redirect("/DashBoard.aspx");
        }
    }

    protected void BTNLogin_Click(object sender, EventArgs e)
    {
        //Validation Rules to Check if All Fields are entered
        if (TBUsername.Text.Equals("") && TBPassword.Text.Equals(""))
        {
            FieldValidator.Visible = true;
            FieldValidator.Text = "Username & Password field is required!";

        } else if (TBPassword.Text.Equals(""))
        {
            FieldValidator.Visible = true;
            FieldValidator.Text = "Password field is required!";

        } else if (TBUsername.Text.Equals("")) {

            FieldValidator.Visible = true;
            FieldValidator.Text = "Username field is required!";

        } else
        {
            //If Validations Pass Through, Reset Field Validator
            FieldValidator.Visible = true;
            FieldValidator.Text = "";

            //Attempt to Decrypt Password From DB and Match
            AuthenticationCheck(TBUsername.Text.ToString(), TBPassword.Text.ToString());
        }

    }

    private void AuthenticationCheck(string username, string password)
    { 
        //Installise New User
        User user = null;

        //Auto Disposable Method
        using (MSAAnalyserEntities context = new MSAAnalyserEntities())
        {
            //Check Against Database
            user = context.Users.FirstOrDefault(u => u.UserName == username);
        }

        //If the User does not Equal to Null
        if (user != null)
        {
            //Attempt to Decrypt the Password from Database and at the same time check if it matches with the User Input
            if (AESCrypt.Decrypt(user.PassWord) == password)
            {
                //Display Sucess Message
                FieldValidator.CssClass = "alert-success";
                FieldValidator.Visible = true;
                FieldValidator.Text = "Success! Redirecting you to the control panel...";

                //Create Session and redirect user to DashBoard
                Session["UserName"] = user.UserName;
                Response.Redirect("/DashBoard.aspx");

            } else
            {
                //Display Error
                FieldValidator.Visible = true;
                FieldValidator.Text = "Username or Password was Incorrect!";
            }

        } else
        {
            //Display Error
            FieldValidator.Visible = true;
            FieldValidator.Text = "Username or Password was Incorrect!";
        }
    }

   
}