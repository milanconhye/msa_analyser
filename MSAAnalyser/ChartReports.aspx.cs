﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ChartReports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Checks Weather a Session Exists by checking UserName
            if (Session["UserName"] != null)
            {

                //Installise User from ADO and Store Session Username to String
                User user = null;
                string username = Session["UserName"].ToString();

                //Get First Name and Last name from Database using the stored data in session
                using (MSAAnalyserEntities context = new MSAAnalyserEntities())
                {
                    user = context.Users.FirstOrDefault(r => r.UserName == username);
                }
            }
            else
            {
                //Redirect user to Login Page
                Response.Redirect("/Default.aspx");
            }

        }
        catch (NullReferenceException)
        {
            //Redirect user to Login Page
            Response.Redirect("/Default.aspx");
        }
    }

    protected void ByOrganisation_Click(object sender, EventArgs e)
    {
        Response.Redirect("./ByOrganisation.aspx");
    }

    protected void logOutButton_Click(object sender, EventArgs e)
    {
        //Kill Session
        Session.Abandon();

        //Redirect user to Login Page
        Response.Redirect("/Default.aspx");

    }
}