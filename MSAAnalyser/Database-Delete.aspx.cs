﻿//Required Imports
using System;
using System.Linq;
using System.Web.UI.HtmlControls;
using Function;

//Page Class - Holds all Events and Methods
public partial class DatabaseDelete : System.Web.UI.Page
{
    //Page Load Event
    protected void Page_Load(object sender, EventArgs e)
    {
        //If Its not a PostBack (Page Refresh)
        if (!this.IsPostBack)
        {

            //Checks Weather a Session Exists by checking UserName
            if (Session["UserName"] != null)
            {
                //Auto Disaposable Method
                using (MSAAnalyserEntities context = new MSAAnalyserEntities())
                {
                    //Store Session Object to String
                    string username = Session["UserName"].ToString();

                    //Find and Select Username that is currently logged in
                    User user = context.Users.FirstOrDefault(r => r.UserName == username);

                    //Check If user is an Admin or not
                    if (user.isAdmin)
                    {
                        //Reset Values and Load Dates
                        DisplayPanel.Visible = false;
                        btnDelete.Enabled = false;
                        Functions.LoadDates(DropDownRecordYear);
                    }
                    else
                    {
                        //Redirect to Dashboard if not admin 
                        Response.Redirect("/Dashboard.aspx");
                    }
                }

            }
            else
            {
                //Redirect to Login Page
                Response.Redirect("/Default.aspx");
            }
        }
    }

    
    //When the Record Year DropDownList has Changed
    protected void DropDownRecordYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Hide Panel
        DisplayPanel.Visible = false;

        //If the Record Year is at is Default Value - Text and Selected used to minimise bugs
        if (DropDownRecordYear.Text == "Select Value" || DropDownRecordYear.SelectedValue == "Select Value")
        {
            //Hide Panel
            DisplayPanel.Visible = false;

        }
        else
        {
            //Display Panel and Display List of Universities
            DisplayPanel.Visible = true;
            Functions.UpdateUniversityDataField(DropDownListUniversities, DropDownRecordYear);
        }
    }

    protected void logOutButton_Click(object sender, EventArgs e)
    {
        //Kill Session
        Session.Abandon();

        //Redirect user to Login Page
        Response.Redirect("/Default.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //Refresh Page
        Response.Redirect(Request.RawUrl);
    }

    protected void btnDelete_Click(object sender, EventArgs e)
    {
        //Auto Disposable Method
        using (MSAAnalyserEntities context = new MSAAnalyserEntities())
        {
            try
            {
                //Find and Select Table Record - Organisation Name
                TableRecord tableRecord = context.TableRecords.FirstOrDefault(tr => tr.OrganisationName == DropDownListUniversities.SelectedValue);
                
                //Select Criterias belonging to a Table Record
                TableRecordCriteria tableRecordCriteria = context.TableRecordCriterias.FirstOrDefault(trc => trc.RecordId == tableRecord.Id);
                
                //Remove from TableRecordSubCriteria if Any
                if (context.TableRecordSubCriterias.Where(trsc => trsc.RecordRecordId == tableRecord.Id) != null)
                {
                    context.TableRecordSubCriterias.RemoveRange(context.TableRecordSubCriterias.Where(trsc => trsc.RecordRecordId == tableRecord.Id));
                }

                //Remove All current TableRecordCriteria Records
                context.TableRecordCriterias.RemoveRange(context.TableRecordCriterias.Where(trc => trc.RecordId == tableRecord.Id));

                //Delete From TableRecord and Commit Changes
                context.TableRecords.Remove(tableRecord);
                context.SaveChanges();

                //Show user Sucesss Message
                FieldValidator.CssClass = "alert-success";
                FieldValidator.Visible = true;
                FieldValidator.Text = DropDownListUniversities.SelectedItem.Text + " has successfully been deleted!";

                //Refresh Page in 2.5 Seconds
                HtmlMeta meta = new HtmlMeta();
                meta.HttpEquiv = "Refresh";
                meta.Content = "2.5;url=Database-Delete.aspx";
                this.Page.Controls.Add(meta);

            }
            catch (Exception ex)
            {
                //Catch Exception
                string innerMessage = (ex.InnerException != null)? ex.InnerException.Message : "";
                Console.WriteLine(innerMessage);
            }
        }
    }

    protected void BtnSelect_Click(object sender, EventArgs e)
    {
        //If there is A Value in Database then enable Delete Button, otherwise Display Alert
        if (DropDownListUniversities.SelectedValue != "")
        {
            btnDelete.Enabled = true;
        }
        else
        {
            //Display Error Message
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No Organisations exist for this Record Year!');", true);
        }
    }
}