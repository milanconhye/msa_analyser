﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CriteriaView.aspx.cs" Inherits="CriteriaView" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Database Criteria View - MSA Analyser</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <link rel="stylesheet" href="Content/Site.css" />
    <link rel="stylesheet" href="Content/bootstrap.css" />

    <script src="Scripts/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <style type="text/css">
        .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
            width: 15%;
            margin: 10px;
            color: #fff;
            background-color: #337ab7;
        }

            .nav-pills > li.active > a, .nav-pills > li.active > a:focus, .nav-pills > li.active > a:hover {
                width: 100%;
                margin: 10px;
                color: #fff;
                background-color: #337ab7;
            }


        .auto-style25 {
            color: inherit;
            font-weight: 200;
            line-height: 2.1428571435;
            text-align: left;
            margin-left: 40px;
            margin-right: 40px;
            margin-top: 0px;
            margin-bottom: 30px;
            padding: 38px 20px;
            background-color: #eeeeee;
            width: 95%;
        }

        .auto-style32 {
            width: 52%;
        }

        .auto-style36 {
            font-size: large;
        }

        .auto-style37 {
            font-size: large;
            text-decoration: underline;
        }

        .auto-style38 {
            width: 320px;
            padding-right: 10px;
        }

        .auto-style39 {
            width: 89px;
        }

        .auto-style40 {
            color: #a94442;
            font-size: large;
            border-color: #ebccd1;
            background-color: #f2dede;
        }

        .auto-style41 {
            width: 191px;
        }

        .auto-style42 {
            width: 60%;
        }

        .auto-style43 {
            width: 115px;
        }

        .auto-style44 {
            width: 69px;
        }

        .auto-style45 {
            text-decoration: underline;
        }

        label {
            margin-right: 25px;
        }

        .auto-style46 {
            width: 202px;
            height: 101px;
        }

        #TAAssesment {
            width: 358px;
        }

        .auto-style49 {
            width: 350px;
            font-size: medium;
        }

        .auto-style50 {
            color: #FF3300;
        }

        .hidden {
            display: none;
        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #f3f3f3;
        }

        li {
            float: left;
        }

            li a {
                display: block;
                color: black;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

                li a:hover:not(.active) {
                    background-color: #1d8cba;
                    text-decoration: none;
                    color: white;
                }

                li a:hover {
                    text-decoration: none;
                    color: white;
                }

        .active {
            background-color: #1d8cba;
            color: white;
        }

        a {
           text-decoration: none;
        }


    </style>

    <script type="text/javascript">
        $().ready(function () {
            $('.cls').change(function () {
                $('#phide').hide();
            });
        });

        function ShowHideDiv(CBhasCriteria) {

            var dvSubCriteria = document.getElementById("dvSubCriteria");

            if (CBhasCriteria.checked == false) {

                if (confirm("Warning: You have unselected the Criteria, this will remove all SubCriterias associated with the Criteria and it self, do you wish to proceed?")) {
                    dvSubCriteria.style.display = CBhasCriteria.checked ? "block" : "none";
                    return true;

                } else {
                    CBhasCriteria.checked = true;
                    return false;
                }

            } else {
                dvSubCriteria.style.display = CBhasCriteria.checked ? "block" : "none";
            }
        }

        function UpdateMsg() {
            var dropDownUniversities = document.getElementById("<%=DropDownListUniversity.ClientID%>").options[document.getElementById("<%=DropDownListUniversity.ClientID%>").selectedIndex].text;
            alert(dropDownUniversities + " has been updated!");
            window.location.href = window.location.href + "?rnd=" + Math.random();
        }

        function ClearSession() {
            document.getElementById("ButtonLogOut").click();
        }

    </script>

</head>
<body>
    <form id="form1" runat="server">
        <div class="auto-style25">

            <div style="width: auto; height: auto; font-size: 14px;">
            <ul>
                <li><a href="Dashboard.aspx">DashBoard</a></li>
                <li><a href="Database-Add.aspx">Database View</a></li>
                <li><a href="CriteriaView.aspx" class="active">Criteria View</a></li>
                <li><a href="#" onclick="ClearSession()">Sign Out</a></li>
            </ul>
            <asp:Button ID="ButtonLogOut" runat="server" CausesValidation="False" OnClick="logOutButton_Click" CssClass="hidden"></asp:Button>
            </div>

            <br />
            <span class="auto-style37">Criteria View</span><br />

            <asp:Label ID="FieldValidator" runat="server" CssClass="auto-style40" Visible="False"></asp:Label>


            <br />
            <br />

            <table class="auto-style32">
                <tr>
                    <td class="auto-style44">Record Date<span class="auto-style50">*</span>:</td>
                    <td class="auto-style39">
                        <asp:DropDownList ID="DropDownRecordYear" runat="server" Height="25px" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="DropDownRecordYear_SelectedIndexChanged"></asp:DropDownList>
                    </td>
                </tr>
            </table>

            <table class="auto-style42">
                <tr>
                    <td class="auto-style41">
                        <asp:Label ID="Label2" runat="server" Text="Organisation Name:"></asp:Label>
                    </td>
                    <td class="auto-style43">
                        <asp:DropDownList ID="DropDownListUniversity" runat="server" Height="25px" Width="150px" class="cls">
                        </asp:DropDownList>
                    </td>
                    <td>&nbsp;
                                <asp:Button ID="BtnUniSelect" runat="server" OnClick="BtnUniSelect_Click" Text="Select" CssClass="btn-success active" Height="25px" Style="font-size: 10px" />
                    </td>
                </tr>
            </table>

            <br />

            <div id="phide">
                <asp:Panel ID="Panel" runat="server">
                    <asp:Repeater ID="rptCriteria" runat="server">
                        <ItemTemplate>
                            <asp:Button ID="Tab" runat="server" OnClick="TabName_Click" CssClass="btn-success" Text='<%# Container.DataItem %>'></asp:Button>
                        </ItemTemplate>
                        <FooterTemplate>
                        </FooterTemplate>
                    </asp:Repeater>
                    <br />
                </asp:Panel>

                <br />

                <asp:Panel ID="CriteriaPanelAll" runat="server">


                    <div>



                        <asp:Label ID="lblCriteriaDB" runat="server" CssClass="auto-style36" Font-Underline="True"></asp:Label>


                        <br />

                        <asp:Panel ID="CriteriaPanel" runat="server" Visible="false">

                            <table style="width: 100%;">
                                <tr>
                                    <td class="auto-style38">
                                        <asp:Label ID="LblhasCriteria" runat="server"></asp:Label>
                                    </td>
                                    <td>

                                        <input type="checkbox" id="CBhasCriteria" runat="server" onclick="ShowHideDiv(this)" />
                                    </td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style38">
                                        <asp:Label ID="LblAssesment" runat="server" Text="Assesment:"></asp:Label>
                                    </td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style38">
                                        <textarea id="TAAssesment" runat="server" class="auto-style46" name="TAAssesment"></textarea></td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                            <br />


                            <div id="dvSubCriteria" style="<%= DefinirVisibilidad() %>">
                                <span class="auto-style45">Sub Criterias</span>:
                            <asp:CheckBoxList ID="CheckBoxSubCriteria" runat="server" RepeatColumns="4" RepeatDirection="Horizontal">
                            </asp:CheckBoxList>
                            </div>

                            <br />

                            <table>
                                <tr>
                                    <td class="auto-style48">
                                        <asp:Button ID="btnUpdate" runat="server" CssClass="btn-warning active" OnClick="btnUpdate_Click" Text="Update Record" />
                                    </td>
                                    <td>&nbsp;<asp:Button ID="btnCancel" runat="server" AutoPostBack="true" CssClass="btn-primary" OnClick="btnCancel_Click" Text="Cancel" />
                                        &nbsp;</td>
                                </tr>
                            </table>
                            <br />

                        </asp:Panel>



                    </div>
                </asp:Panel>

            </div>
        </div>
    </form>
    <hr />
    <footer style="margin-left: 40px;">
        <p>Copyright &copy; <%: DateTime.Now.Year %> MSA Analyser | University of Greenwich | Developed by <a href="http://www.milanconhye.com" target="_blank">Milan Conhye</a> &amp; <a href="https://www.gre.ac.uk/ach/study/cis/gwizards/home" target="_blank">GWIZARDS</a></p>
        <span id="siteseal"><script async="async" type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=v1vRkL5x8BW6FacK2b2Yv6eMP7l59wtHyG1BFhpzgial5cD3287aGd5eVg8r"></script></span>
         <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async="async"></script>
        <img src="mcafee.png" alt="MCAFEE" style="width:8%; height:60%" />
    </footer>
</body>
</html>
