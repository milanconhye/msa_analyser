﻿//Required Imports
using System;
using System.Linq;

//Page Class - Holds all Events and Methods
public partial class Dashboard : System.Web.UI.Page
{
    //Page Load Event
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Checks Weather a Session Exists by checking UserName
            if (Session["UserName"] != null)
            {

                //Installise User from ADO and Store Session Username to String
                User user = null;
                string username = Session["UserName"].ToString();

                //Get First Name and Last name from Database using the stored data in session
                using (MSAAnalyserEntities context = new MSAAnalyserEntities())
                {
                    user = context.Users.FirstOrDefault(r => r.UserName == username);
                }
                    
                //Print out First Name and Last Name onto Welcome Message
                TBWelcome.Text = "Welcome " + user.FirstName + " " + user.LastName + ",";

            }
            else
            {
                //Redirect user to Login Page
                Response.Redirect("/Default.aspx");
            }

        }
        catch (NullReferenceException)
        {
            //Redirect user to Login Page
            Response.Redirect("/Default.aspx");
        }
    }

    protected void logOutButton_Click(object sender, EventArgs e)
    {
        //Kill Session
        Session.Abandon();

        //Redirect user to Login Page
        Response.Redirect("/Default.aspx");

    }

    protected void UniversityDatabaseLink_Click(object sender, EventArgs e)
    {
        //Redirect User to Database Add
        Response.Redirect("/Database-Add.aspx");
    }

    protected void ChartsReportsBtn_Click(object sender, EventArgs e)
    {
        Response.Redirect("./ChartReports.aspx");
    }
}