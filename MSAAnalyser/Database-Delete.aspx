﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Database-Delete.aspx.cs" Inherits="DatabaseDelete" %>

<%@ Register Assembly="EditableDropDownList" Namespace="EditableControls" TagPrefix="editable" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Database (Delete) - MSA Analyser</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <link rel="stylesheet" href="Content/Site.css" />
    <link rel="stylesheet" href="Content/bootstrap.css" />

    <script src="Scripts/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.button.js" type="text/javascript"></script>
    <script src="js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="js/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.ui.combobox.js" type="text/javascript"></script>

    <style type="text/css">
        .auto-style25 {
            color: inherit;
            font-weight: 200;
            line-height: 2.1428571435;
            text-align: left;
            margin-left: 40px;
            margin-right: 40px;
            margin-top: 0px;
            margin-bottom: 30px;
            padding: 38px 20px;
            background-color: #eeeeee;
            width: 95%;
        }

        body {
            font-family: Arial;
            font-size: 14px;
        }

        table {
            border: 1px solid #ccc;
            width: 450px;
            margin-bottom: -1px;
        }

            table th {
                background-color: #F7F7F7;
                color: #333;
                font-weight: bold;
            }

            table th, table td {
                padding: 5px;
                border-color: #ccc;
            }

        textarea {
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
            min-width: 110px;
            max-width: 135px;
        }

        .auto-style29 {
            color: #a94442;
            font-size: large;
            border-color: #ebccd1;
            background-color: #f2dede;
        }

        .auto-style46 {
            width: 114px;
        }

        .auto-style49 {
            width: 40%;
        }

        .auto-style51 {
            text-align: left;
        }

        table {
            border: 0px solid #ccc;
            width: 450px;
            margin-bottom: -1px;
        }

        .auto-style54 {
            width: 91px;
            height: 68px;
        }

        .auto-style55 {
            width: 53px;
            height: 68px;
        }

        .auto-style56 {
            width: 87%;
        }

        .auto-style58 {
            width: 613px;
        }

        .auto-style60 {
            width: 38%;
        }

        .auto-style61 {
            height: 39px;
            width: 20px;
        }

        .auto-style63 {
            width: 491px;
        }

        .auto-style64 {
            width: 164px;
        }

        .red {
            color: #FF0000;
        }

        .auto-style65 {
            width: 350px;
            font-size: medium;
        }

        .auto-style66 {
            width: 49px;
            color: #000000;
        }

        .hidden {
            display: none;
        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #f3f3f3;
        }

        li {
            float: left;
        }

            li a {
                display: block;
                color: black;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

                li a:hover:not(.active) {
                    background-color: #1d8cba;
                    text-decoration: none;
                    color: white;
                }

                li a:hover {
                    text-decoration: none;
                    color: white;
                }

        .active {
            background-color: #1d8cba;
            color: white;
        }

        a {
           text-decoration: none;
        }


    </style>

    <script type="text/javascript">
        $().ready(function () {
            $('.cls').change(function () {
                $('#btnDelete').prop("disabled", true);
            });
        });

        function ClearSession() {
            document.getElementById("ButtonLogOut").click();
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" />

        <div class="auto-style25">

            <div style="width: auto; height: auto; font-size: 14px;">
            <ul>
                <li><a href="Dashboard.aspx">DashBoard</a></li>
                <li><a href="Database-Add.aspx" class="active">Database View</a></li>
                <li><a href="CriteriaView.aspx">Criteria View</a></li>
                <li><a href="#" onclick="ClearSession()">Sign Out</a></li>
            </ul>
            <asp:Button ID="ButtonLogOut" runat="server" CausesValidation="False" OnClick="logOutButton_Click" CssClass="hidden"></asp:Button>
            </div>

            <br />

            <table class="auto-style61">
                <tr>
                    <td class="auto-style58">

                        <asp:Button ID="BtnAddRecord" runat="server" CssClass="btn-primary active" PostBackUrl="~/Database-Add.aspx" Text="Add Record" />

                    </td>

                    <td class="auto-style58">

                        <asp:Button ID="BtnEditRecord" runat="server" CssClass="btn-primary active" PostBackUrl="~/Database-Edit.aspx" Text="Edit Record" />

                    </td>
                    <td class="auto-style60">

                        <asp:Button ID="BtnDeleteRecord" runat="server" CssClass="btn-primary active focus" Text="Delete Record" PostBackUrl="~/Database-Delete.aspx" />

                    </td>
                </tr>
            </table>


            <br />

            <asp:Label ID="FieldValidator" runat="server" CssClass="auto-style29" Visible="False"></asp:Label>

            <br />


            <table class="auto-style49">
                <tr>
                    <td class="auto-style54">Record Date<span class="red">*</span>:</td>
                    <td class="auto-style55">

                        <asp:DropDownList ID="DropDownRecordYear" runat="server" Height="25px" Width="150px" OnSelectedIndexChanged="DropDownRecordYear_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>

                    </td>
                </tr>
            </table>

            <asp:Panel ID="DisplayPanel" runat="server">
                <table class="auto-style49">
                    <tr>
                        <td class="auto-style54"><span><span class="auto-style66">Organisation Name</span></span><span class="red">*</span>:</td>
                        <td class="auto-style55">
                            <asp:DropDownList ID="DropDownListUniversities" runat="server" class="cls" Height="25px" Width="150px">
                            </asp:DropDownList>
                            <asp:Button ID="BtnSelect" runat="server" CssClass="btn-success active" Height="25px" OnClick="BtnSelect_Click" Style="font-size: 10px" Text="Select" />
                        </td>
                    </tr>


                </table>


                <table class="auto-style56">
                    <tr>
                        <td class="auto-style64">&nbsp;</td>
                        <td class="auto-style63">&nbsp;<asp:Button ID="btnDelete" runat="server" CssClass="btn-danger active" Text="Delete Record" OnClientClick="return confirm('Are you sure you want to delete this record?')" OnClick="btnDelete_Click" />
                            &nbsp;<asp:Button ID="btnCancel" runat="server" CssClass="btn-primary" OnClick="btnCancel_Click" Text="Cancel" />
                        </td>
                        <td class="auto-style46">&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </asp:Panel>
        </div>
        <hr />
        <footer style="margin-left: 40px;">
        <p>Copyright &copy; <%: DateTime.Now.Year %> MSA Analyser | University of Greenwich | Developed by <a href="http://www.milanconhye.com" target="_blank">Milan Conhye</a> &amp; <a href="https://www.gre.ac.uk/ach/study/cis/gwizards/home" target="_blank">GWIZARDS</a></p>
        <span id="siteseal"><script async="async" type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=v1vRkL5x8BW6FacK2b2Yv6eMP7l59wtHyG1BFhpzgial5cD3287aGd5eVg8r"></script></span>
         <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async="async"></script>
        <img src="mcafee.png" alt="MCAFEE" style="width:8%; height:60%" />
        </footer>
    </form>
</body>
</html>
