﻿//Required Imports
using System;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using Function;

//Page Class - Holds all Events and Methods
public partial class CriteriaView : System.Web.UI.Page
{
    //Page Load Event
    protected void Page_Load(object sender, EventArgs e)
    {
        //If Its not a PostBack (Page Refresh)
        if (!this.IsPostBack)
        {
            //Checks Weather a Session Exists by checking UserName
            if (Session["UserName"] != null)
            {
                //Load Database Dates onto the DropDownList Record Year
                Functions.LoadDates(DropDownRecordYear);
            }
            else
            {
                //Redirection to Login Page
                Response.Redirect("/Default.aspx");
            }
        }
    }

    //When the Item is changed on the Drop Down Year
    protected void DropDownRecordYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Hide All Panels
        CriteriaPanelAll.Visible = false;
        CriteriaPanel.Visible = false;
        Panel.Visible = false;

        //If the default items have been selected 
        if (DropDownRecordYear.Text == "Select Value" || DropDownRecordYear.SelectedValue == "Select Value")
        {
            //Hide All Panels
            CriteriaPanelAll.Visible = false;
            CriteriaPanel.Visible = false;
            Panel.Visible = false;

        }
        else
        {
            //Update DropDownList Organisation Field
            Functions.UpdateUniversityDataField(DropDownListUniversity, DropDownRecordYear);
        }
    }

    //When the Select Button is pressed
    protected void BtnUniSelect_Click(object sender, EventArgs e)
    {
        //If the Selected Value is not Blank
        if (DropDownListUniversity.SelectedValue != "")
        {
            //Store the Selected Value into a String
            string selectedUniversity = DropDownListUniversity.SelectedValue;

            //Make Panels Visible
            CriteriaPanelAll.Visible = false;
            Panel.Visible = true;

            //Load All Criterias and Place them on to a Menu
            Functions.LoadCriteriaMenu(DropDownListUniversity.SelectedValue, rptCriteria);
        }
        else
        {
            //Display Alert if Selected Value is blank
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No Organisations exist for this Record Year!');", true);
        }

    }

    //For Each Tab that is selected within the Repeater
    protected void TabName_Click(object sender, EventArgs e)
    {
        //Get Clicked Button Text and store in String
        string criteriaClicked = (sender as Button).Text;

        //Change CSS Style when Button is Clicked
        (sender as Button).CssClass = "btn-success active";
        
        //Store String to Criteria Title
        lblCriteriaDB.Text = criteriaClicked;

        //Make Panels Visible
        CriteriaPanelAll.Visible = true;
        CriteriaPanel.Visible = true;

        //Auto Disaposable Method
        using (MSAAnalyserEntities context = new MSAAnalyserEntities())
        {
            //Select Table Record from Selected Organisation Name DropDownList
            TableRecord tableRecord = context.TableRecords.FirstOrDefault(r => r.OrganisationName == DropDownListUniversity.SelectedValue);

            //Select Table Criteria from the Tab Menu that was clicked
            TableCriteria tableCriteria = context.TableCriterias.FirstOrDefault(r => r.CriteriaName == criteriaClicked);

            //Select TableRecord Criteria from TableRecord ID and Where Criteria ID Matches
            TableRecordCriteria tableRecordCriteria = context.TableRecordCriterias.FirstOrDefault(trc => trc.RecordId == tableRecord.Id && trc.CriteriaId == tableCriteria.Id);

            //If table Records does not produce a null result
            if (tableRecordCriteria != null)
            {
                //Display info on Label
                LblhasCriteria.Text = "Does " + DropDownListUniversity.SelectedValue + " have " + tableCriteria.CriteriaName + " ?";

                //Check Weather Organisation Has Criteria
                CBhasCriteria.Checked = tableRecordCriteria.CriteriaReported;

                //Load Assesment From DB
                TAAssesment.Value = tableRecordCriteria.Commentary;

                //Now Load all Sub Criterias Belonging to the Specific Criteria
                Functions.LoadSubCriteriaList(tableCriteria, CheckBoxSubCriteria);

                //Check to see which SubCriteria is Checked
                CheckBoxList CriteriaCheckBox = (CheckBoxList)this.FindControl("CheckBoxSubCriteria");

                //Install TableSubCriteria
                TableSubCriteria tableSubCriteria;

                //For Each SubCriteria that is selected in Database
                foreach (ListItem item in CriteriaCheckBox.Items)
                {
                    //Find the SubCriteria using the Criteria Name
                    tableSubCriteria = context.TableSubCriterias.FirstOrDefault(tsc => tsc.SubCriteriaName == item.Value);

                    //Installise TableRecordSubCriteria and find where Record ID and Sub Critria ID Matches
                    var tableRecordSubCriteria = context.TableRecordSubCriterias.Where(recordType => recordType.RecordCriteriaId == tableRecordCriteria.Id && recordType.SubCriteriaId == tableSubCriteria.Id).FirstOrDefault();

                    //If TableRecordSubCriteria does not produce a null result
                    if (tableRecordSubCriteria != null)
                    {
                        //If the Item is selected in the SubCriteria DB
                        if (tableRecordSubCriteria.Selected == true)
                        {
                            //Select Item in Designer View
                            item.Selected = tableRecordSubCriteria.Selected;

                        }
                        else
                        {
                            //Dont Select Item in Designer View
                            item.Selected = false;
                        }
                    }

                }
            }
        }
    }

    //When the Update Button is Clicked
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        //Auto Disaposable Method
        using (MSAAnalyserEntities context = new MSAAnalyserEntities())
        {
            //Select Table Record from Selected Organisation Name DropDownList
            TableRecord tableRecord = context.TableRecords.FirstOrDefault(tr => tr.OrganisationName == DropDownListUniversity.SelectedValue);

            //Select Table Criteria from the Tab Clicked
            TableCriteria tableCriteria = context.TableCriterias.FirstOrDefault(tc => tc.CriteriaName == lblCriteriaDB.Text);

            //Select TableRecord Criteria from TableRecord ID and Where Criteria ID Matches
            TableRecordCriteria tableRecordCriteria = context.TableRecordCriterias.FirstOrDefault(trc => trc.RecordId == tableRecord.Id && trc.CriteriaId == tableCriteria.Id);

            //We Check if the Criteria is still Selected
            if (CBhasCriteria.Checked == true)
            {
                //Installise New Record SubCriteria
                TableRecordSubCriteria tableRecordSubCriteria = new TableRecordSubCriteria();

                //Remove from TableRecordSubCriteria if Any
                if (context.TableRecordSubCriterias.Where(trsc => trsc.RecordRecordId == tableRecord.Id) != null)
                {
                    context.TableRecordSubCriterias.RemoveRange(context.TableRecordSubCriterias.Where(trsc => trsc.RecordRecordId == tableRecord.Id));
                }

                //Install Table Sub Criteria
                TableSubCriteria tableSubCriteria;

                //Check if any Sub Criterias have been selected 
                if (CheckBoxSubCriteria.Items.Cast<ListItem>().Any(i => i.Selected))
                {
                    //Installise and Find Sub Criteria CheckBox List
                    CheckBoxList SubCriteriaCheckBox = (CheckBoxList)this.FindControl("CheckBoxSubCriteria");

                    //If the Items within the List is Greater then 0
                    if (SubCriteriaCheckBox.Items.Count > 0)
                    {
                        //For Eeach Item in the SubCriteria 
                        foreach (ListItem item in SubCriteriaCheckBox.Items)
                        {
                            //If the item a single Item is Selected 
                            if (item.Selected)
                            {
                                //Look for Item Checked in TableSubCriteria
                                tableSubCriteria = context.TableSubCriterias.FirstOrDefault(r => r.SubCriteriaName == item.Value);

                                try
                                {
                                    //Parse Fields from One Table to another
                                    tableRecordSubCriteria.RecordCriteriaId = tableRecordCriteria.Id;
                                    tableRecordSubCriteria.RecordRecordId = tableRecord.Id;
                                    tableRecordSubCriteria.SubCriteriaId = tableSubCriteria.Id;
                                    tableRecordSubCriteria.Selected = item.Selected;

                                    //Fill in Commentry Field on Database
                                    tableRecordCriteria.Commentary = TAAssesment.Value;

                                    //Add Record to Database and Save Changes
                                    context.TableRecordSubCriterias.Add(tableRecordSubCriteria);
                                    context.SaveChanges();

                                    //Display Alert Message
                                    ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage", "UpdateMsg()", true);

                                }
                                catch (Exception ex)
                                {
                                    //Catch Exception
                                    Console.WriteLine(ex);

                                }

                            }

                        }

                    }

                }
                else
                {
                    //If no Sub Criterias have been checked - Display Error
                    FieldValidator.CssClass = "alert-danger";
                    FieldValidator.Visible = true;
                    FieldValidator.Text = "You must have at least One Sub Criteria Selected!";
                }

            }
            else
            {
                //If HASCriteria CheckBox is Unselected and Update Button has been pressed

                //Remove from TableRecordSubCriteria if Any
                if (context.TableRecordSubCriterias.Where(trsc => trsc.RecordRecordId == tableRecord.Id) != null)
                {
                    context.TableRecordSubCriterias.RemoveRange(context.TableRecordSubCriterias.Where(trsc => trsc.RecordRecordId == tableRecord.Id));
                }

                //Remove Particular current TableRecordCriteria Records and Save Changes
                context.TableRecordCriterias.Remove(tableRecordCriteria);
                context.SaveChanges();

                //Refresh Page
                Response.Redirect(Request.RawUrl);
            }
        }  
    }
    
    //If the Log out button is pressed
    protected void logOutButton_Click(object sender, EventArgs e)
    {
        //Kill Session
        Session.Abandon();

        //Redirect user to Login Page
        Response.Redirect("/Default.aspx");
    }

    //If Cancel Button is Pressed
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //Refresh User Page
        Response.Redirect(Request.RawUrl);
    }

    //Bind CheckBox Click and Hide Div Tag if False, show if true.
    protected string DefinirVisibilidad()
    {
        return this.CBhasCriteria.Checked == true ? "display:block" : "display:none";
    }

}