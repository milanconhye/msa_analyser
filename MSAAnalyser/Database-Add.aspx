﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Database-Add.aspx.cs" Inherits="DatabaseAdd" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Database (Add) - MSA Analyser</title>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <link rel="stylesheet" href="Content/Site.css" />
    <link rel="stylesheet" href="Content/bootstrap.css" />

    <script src="Scripts/bootstrap.min.js"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />

    <link href="css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="js/jquery-1.6.4.min.js" type="text/javascript"></script>
    <script src="js/jquery.ui.core.js" type="text/javascript"></script>
    <script src="js/jquery.ui.widget.js" type="text/javascript"></script>
    <script src="js/jquery.ui.button.js" type="text/javascript"></script>
    <script src="js/jquery.ui.position.js" type="text/javascript"></script>
    <script src="js/jquery.ui.autocomplete.js" type="text/javascript"></script>
    <script src="js/jquery.ui.combobox.js" type="text/javascript"></script>

    <style type="text/css">
        .auto-style25 {
            color: inherit;
            font-weight: 200;
            line-height: 2.1428571435;
            text-align: left;
            margin-left: 40px;
            margin-right: 40px;
            margin-top: 0px;
            margin-bottom: 30px;
            padding: 38px 20px;
            background-color: #eeeeee;
            width: 95%;
        }

        body {
            font-family: Arial;
            font-size: 14px;
        }

        table {
            border: 1px solid #ccc;
            width: 450px;
            margin-bottom: -1px;
        }

            table th {
                background-color: #F7F7F7;
                color: #333;
                font-weight: bold;
            }

            table th, table td {
                padding: 5px;
                border-color: #ccc;
            }

        textarea {
            font-family: inherit;
            font-size: inherit;
            line-height: inherit;
            min-width: 110px;
            max-width: 135px;
        }

        .auto-style29 {
            color: #a94442;
            font-size: large;
            border-color: #ebccd1;
            background-color: #f2dede;
        }

        .auto-style33 {
            height: 39px;
        }

        .auto-style34 {
            height: 39px;
            width: 539px;
        }

        .auto-style35 {
            text-decoration: underline;
        }

        .auto-style36 {
            height: 70px;
        }

        .auto-style40 {
            width: 200px;
        }

        .auto-style41 {
            height: 39px;
            width: 256px;
        }

        .auto-style42 {
            width: 178px;
        }

        .auto-style43 {
            height: 39px;
            width: 178px;
        }

        .auto-style44 {
            min-width: 256px;
            height: 90px;
        }

        .auto-style46 {
            width: 114px;
        }

        .auto-style47 {
            width: 163px;
        }

        .auto-style48 {
            width: 407px;
        }

        .auto-style49 {
            width: 40%;
        }

        .auto-style52 {
            width: 53px;
        }

        table {
            border: 0px solid #ccc;
            width: 450px;
            margin-bottom: -1px;
        }

        .auto-style55 {
            width: 53px;
            height: 68px;
        }

        .auto-style56 {
            width: 87%;
        }

        .auto-style58 {
            text-align: center;
            width: 61px;
            height: 44px;
        }

        .auto-style60 {
            text-align: center;
            width: 487px;
            height: 44px;
        }

        .auto-style61 {
            width: 219px;
        }

        .auto-style62 {
            width: 163px;
            height: 39px;
        }

        .auto-style63 {
            width: 407px;
            height: 39px;
        }

        .auto-style64 {
            width: 114px;
            height: 39px;
        }

        .red {
            color: #FF0000;
        }

        .auto-style67 {
            width: 11px;
            height: 68px;
        }

        .auto-style68 {
            width: 11px;
        }

        .auto-style49 {
            width: 40%;
        }

        .auto-style52 {
            width: 53px;
        }


        .auto-style67 {
            width: 0px;
        }
        .auto-style68 {
            color: #000000;
            text-decoration: underline;
        }

        .auto-style69 {
            width: 166px;
        }

        .hidden {
            display: none;
        }

        ul {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #f3f3f3;
        }

        li {
            float: left;
        }

            li a {
                display: block;
                color: black;
                text-align: center;
                padding: 14px 16px;
                text-decoration: none;
            }

                li a:hover:not(.active) {
                    background-color: #1d8cba;
                    text-decoration: none;
                    color: white;
                }

                li a:hover {
                    text-decoration: none;
                    color: white;
                }

        .active {
            background-color: #1d8cba;
            color: white;
        }

        a {
           text-decoration: none;
        }


        .auto-style70 {
            width: 11px;
            color: #000000;
        }


    </style>

    <script type="text/javascript">
        function ShowHideDiv(chkPassport) {
            var dvOtherTxt = document.getElementById("dvOtherTxt");
            dvOtherTxt.style.display = chkOther.checked ? "block" : "none";
        }

        function ClearSession() {
            document.getElementById("ButtonLogOut").click();
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">

        <div class="auto-style25">

            <div style="width: auto; height: auto; font-size: 14px;">
            <ul>
                <li><a href="Dashboard.aspx">DashBoard</a></li>
                <li><a href="Database-Add.aspx" class="active">Database View</a></li>
                <li><a href="CriteriaView.aspx">Criteria View</a></li>
                <li><a href="#" onclick="ClearSession()">Sign Out</a></li>
            </ul>
            <asp:Button ID="ButtonLogOut" runat="server" CausesValidation="False" OnClick="logOutButton_Click" CssClass="hidden"></asp:Button>
            </div>

            <br />

            <table class="auto-style61">
                <tr>
                    <td class="auto-style58">

                        <asp:Button ID="BtnAddRecord" runat="server" CssClass="btn-primary active focus" PostBackUrl="~/Database-Add.aspx" Text="Add Record" />

                    </td>

                    <td class="auto-style58">

                        <asp:Button ID="BtnEditRecord" runat="server" CssClass="btn-primary active" PostBackUrl="~/Database-Edit.aspx" Text="Edit Record" />

                    </td>
                    <td class="auto-style60">

                        <asp:Button ID="BtnDeleteRecord" runat="server" CssClass="btn-primary active" Text="Delete Record" PostBackUrl="~/Database-Delete.aspx" />

                    </td>
                </tr>
            </table>


            <br />


            <asp:Label ID="FieldValidator" runat="server" CssClass="auto-style29" Visible="False"></asp:Label>
            <br />
            <asp:CompareValidator runat="server" Operator="DataTypeCheck" Type="Double" 
 ControlToValidate="txtTurnOver" CssClass="alert-danger" ErrorMessage="Turn Over must be a Number!" ID="CompareValidator1" />

            <br />
            <table class="auto-style49">
                <tr>
                    <td class="auto-style67"><span class="auto-style63">Organisation Name</span><span class="red">*</span>:</td>
                    <td class="auto-style55">

                        <asp:TextBox ID="txtUniversityName" runat="server" Height="25px" Width="150px" CausesValidation="True"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td class="auto-style70">HQ:</td>
                    <td class="auto-style52">
                        <asp:DropDownList ID="DropDownHQ" runat="server" Height="25px" Width="150px">
                            <asp:ListItem>Select Value</asp:ListItem>
                            <asp:ListItem>UK</asp:ListItem>
                            <asp:ListItem>Japan</asp:ListItem>
                            <asp:ListItem>USA</asp:ListItem>
                            <asp:ListItem>France</asp:ListItem>
                            <asp:ListItem>Sweden</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style68"><span class="auto-style35">Sector</span><span class="red">*</span></td>
                    <td class="auto-style52">&nbsp;</td>
                </tr>


            </table>


            <table style="width: 1%;">
                <tr>
                    <td class="auto-style34">
                        <asp:CheckBoxList ID="CBServicesList" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                            <asp:ListItem>Services</asp:ListItem>
                            <asp:ListItem>Charity</asp:ListItem>
                            <asp:ListItem>Health</asp:ListItem>
                            <asp:ListItem>Public Entity</asp:ListItem>
                            <asp:ListItem>Retail/Fashion</asp:ListItem>
                            <asp:ListItem>Cosmetics</asp:ListItem>
                            <asp:ListItem>Finance</asp:ListItem>
                            <asp:ListItem>Construction</asp:ListItem>
                            <asp:ListItem>Cleaning</asp:ListItem>
                            <asp:ListItem>Security</asp:ListItem>
                            <asp:ListItem>University</asp:ListItem>

                        </asp:CheckBoxList>


                    </td>
                    <td class="auto-style33"></td>
                    <td class="auto-style33"></td>
                </tr>
            </table>
            <table>
                <tr>

                    <td style="padding-right: 35px;">

                        <label for="chkOther">
                            &nbsp;<input type="checkbox" id="chkOther" runat="server" onclick="ShowHideDiv(this)" />
                            Other
                        </label>
                    </td>
                    <td>
                        <div id="dvOtherTxt" style="<%= DefinirVisibilidad() %>">
                            <input type="text" id="txtOther" runat="server" style="height: 25px; width: 150px;" />
                        </div>

                    </td>
                </tr>
            </table>
            <table class="auto-style49">
                        <tr>
                            <td class="auto-style67"><span class="auto-style68">Collaboration with External Actors</span></td>
                            <td class="auto-style52">&nbsp;</td>
                        </tr>
                    </table>
            <table style="width: 1%;">
                        <tr>
                            <td class="auto-style34">
                                <asp:CheckBoxList ID="CBCollabList" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                                    <asp:ListItem>HE</asp:ListItem>
                                    <asp:ListItem>HEPA</asp:ListItem>
                                    <asp:ListItem>LUPC</asp:ListItem>
                                    <asp:ListItem>SUPC</asp:ListItem>
                                    <asp:ListItem>NWUPC</asp:ListItem>
                                    <asp:ListItem>HEPCW</asp:ListItem>
                                    <asp:ListItem>NEUPC</asp:ListItem>
                                    <asp:ListItem>UKUPC</asp:ListItem>
                                    <asp:ListItem>CCS</asp:ListItem>
                                    <asp:ListItem>CIPS</asp:ListItem>
                                    <asp:ListItem>NetPositives</asp:ListItem>
                                    <asp:ListItem>Electronic Watch</asp:ListItem>
                                    <asp:ListItem>ETI</asp:ListItem>
                                    <asp:ListItem>Sedex</asp:ListItem>
                                </asp:CheckBoxList>
                            </td>
                            <td class="auto-style33"></td>
                            <td class="auto-style33"></td>
                        </tr>
                    </table>
                    <table>
                        <tr>
                            <td class="auto-style69" style="padding-right: 35px;">Suspected Use of Template: </td>
                            <td>
                                
                                    &nbsp;<asp:DropDownList ID="DropDownSuspect" runat="server" Height="25px" Width="150px">
                                        <asp:ListItem Value="Select Value">Select Value</asp:ListItem>
                                        <asp:ListItem>Whole</asp:ListItem>
                                        <asp:ListItem>Partial</asp:ListItem>
                                    </asp:DropDownList>
                                
                            </td>
                        </tr>
                    </table>

            <table style="width: 25%;">
                <tr>
                    <td class="auto-style40">Size of Statement: </td>
                    <td class="auto-style42">
                        <asp:DropDownList ID="DropDownSizeOfStatement" runat="server" Height="25px" Width="150px">
                            <asp:ListItem>Select Value</asp:ListItem>
                            <asp:ListItem>Less Than 500</asp:ListItem>
                            <asp:ListItem>500 - 1000</asp:ListItem>
                            <asp:ListItem>1000 - 2000</asp:ListItem>
                            <asp:ListItem>More Than 2000</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style40">Turn Over (£):</td>
                    <td class="auto-style42">
                        <asp:TextBox ID="txtTurnOver" runat="server" Height="25px" Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style40">URL:</td>
                    <td class="auto-style42">
                        <asp:TextBox ID="txtURL" runat="server" Height="25px" Width="150px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style40">Signed by Director/Member/Partner?</td>
                    <td class="auto-style42">
                        <asp:CheckBox ID="cbSigned" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style40">Signed by Other:</td>
                    <td class="auto-style42">
                        <asp:DropDownList ID="DropDownSignedByOther" runat="server" Height="25px" Width="150px">
                            <asp:ListItem>Select Value</asp:ListItem>
                            <asp:ListItem>CEO</asp:ListItem>
                            <asp:ListItem>Director</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style40">Home Page?</td>
                    <td class="auto-style42">
                        <asp:CheckBox ID="cbHomePage" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style40">Home Page Other:</td>
                    <td class="auto-style42">
                        <asp:DropDownList ID="DropDownHomePageOther" runat="server" Height="25px" Width="150px">
                            <asp:ListItem>Select Value</asp:ListItem>
                            <asp:ListItem>Bottom</asp:ListItem>
                            <asp:ListItem>Link</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style41">Introduction:</td>
                    <td class="auto-style43">
                        <asp:CheckBox ID="cbIntroduction" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style40">Commentary:</td>
                    <td class="auto-style42">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style40">
                        <textarea id="txtCommentary" class="auto-style44" runat="server" cols="20" name="S1" rows="1"></textarea></td>
                    <td class="auto-style42">&nbsp;</td>
                </tr>
            </table>
            <table style="width: 25%;">
                <tr>
                    <td><span class="auto-style35">Criterias</span><span class="red">*</span></td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
            <table style="width: 25%;">
                <tr>
                    <td class="auto-style36">



                        <asp:CheckBoxList ID="CheckBoxCriteria" runat="server" RepeatColumns="3" RepeatDirection="Horizontal">
                        </asp:CheckBoxList>



                    </td>
                </tr>
            </table>
            <table class="auto-style56">
                <tr>
                    <td class="auto-style47">Record Year<span class="red">*</span>: </td>
                    <td class="auto-style48">
                        <asp:DropDownList ID="DropDownRecordYear" runat="server" Height="25px" Width="150px"></asp:DropDownList>
                    </td>
                    <td class="auto-style46">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style62"></td>
                    <td class="auto-style63">
                        <asp:Button ID="btnAdd" runat="server" Text="Add Record" OnClick="btnAdd_Click" CssClass="btn-success active" />




                        &nbsp;<asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" CssClass="btn-primary" />




                    </td>
                    <td class="auto-style64"></td>
                    <td class="auto-style33"></td>
                </tr>
                <tr>
                    <td class="auto-style47">&nbsp;</td>
                    <td class="auto-style48">&nbsp;</td>
                    <td class="auto-style46">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <hr />
        <footer style="margin-left: 40px;">
        <p>Copyright &copy; <%: DateTime.Now.Year %> MSA Analyser | University of Greenwich | Developed by <a href="http://www.milanconhye.com" target="_blank">Milan Conhye</a> &amp; <a href="https://www.gre.ac.uk/ach/study/cis/gwizards/home" target="_blank">GWIZARDS</a></p>
    <span id="siteseal"><script async="async" type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=v1vRkL5x8BW6FacK2b2Yv6eMP7l59wtHyG1BFhpzgial5cD3287aGd5eVg8r"></script></span>
         <script type="text/javascript" src="https://cdn.ywxi.net/js/1.js" async="async"></script>
        <img src="mcafee.png" alt="MCAFEE" style="width:8%; height:60%" />
        </footer>
    </form>
</body>
</html>
