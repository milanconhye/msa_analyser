﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ByOrganisation : System.Web.UI.Page
{

    //define connection string for connect database   
    string conString = ConfigurationManager.ConnectionStrings["connectionDB"].ConnectionString;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Checks Weather a Session Exists by checking UserName
            if (Session["UserName"] != null)
            {

                //Installise User from ADO and Store Session Username to String
                User user = null;
                string username = Session["UserName"].ToString();

                //Get First Name and Last name from Database using the stored data in session
                using (MSAAnalyserEntities context = new MSAAnalyserEntities())
                {
                    user = context.Users.FirstOrDefault(r => r.UserName == username);
                }

                //Load All Records onto table
                using (SqlConnection con = new SqlConnection(conString))
                {
                    {
                        using (SqlCommand cmd = new SqlCommand("select * from TableRecord"))
                        {
                            SqlDataAdapter dt = new SqlDataAdapter();
                            try
                            {
                                cmd.Connection = con;
                                con.Open();
                                dt.SelectCommand = cmd;

                                DataTable dTable = new DataTable();
                                dt.Fill(dTable);

                                AllOrganisations.DataSource = dTable;
                                AllOrganisations.DataBind();
                            }
                            catch (Exception ex)
                            {
                                //Throw Exception if anything was to go wrong
                                Console.WriteLine(ex);
                            }
                        }
                    }
                }

            }
            else
            {
                //Redirect user to Login Page
                Response.Redirect("/Default.aspx");
            }

        }
        catch (NullReferenceException)
        {
            //Redirect user to Login Page
            Response.Redirect("/Default.aspx");
        }
    }

    protected void logOutButton_Click(object sender, EventArgs e)
    {
        //Kill Session
        Session.Abandon();

        //Redirect user to Login Page
        Response.Redirect("/Default.aspx");

    }
}