﻿//Required Imports
using System;
using System.Linq;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Function;

//Page Class - Holds all Events and Methods
public partial class DatabaseEdit : System.Web.UI.Page
{
    //Page Load Event
    protected void Page_Load(object sender, EventArgs e)
    {
        //If Its not a PostBack (Page Refresh)
        if (!this.IsPostBack)
        {

            //Checks Weather a Session Exists by checking UserName
            if (Session["UserName"] != null)
            {
                //Auto Disaposable Method
                using (MSAAnalyserEntities context = new MSAAnalyserEntities())
                {
                    //Store Session Object to String
                    string username = Session["UserName"].ToString();

                    //Find and Select Username that is currently logged in
                    User user = context.Users.FirstOrDefault(r => r.UserName == username);

                    //Check If user is an Admin or not - if not Hide Button
                    if (!user.isAdmin)
                    {
                        BtnDeleteRecord.Visible = false;
                    }

                }

                //Load Dates into Record Year and Hide Panels
                Functions.LoadDates(DropDownRecordYear);
                DisplayPanel.Visible = false;
                DisplayUniPanel.Visible = false;

            }
            else
            {
                //ReDirect to Login Page
                Response.Redirect("/Default.aspx");
            }

            
        }

    }

    //If the Selected Value from the DropDown Year has Changed
    protected void DropDownRecordYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Hide Panels
        DisplayPanel.Visible = false;
        DisplayUniPanel.Visible = false;

        //If the Drop Down Record Year is at its Default Value
        if (DropDownRecordYear.Text == "Select Value" || DropDownRecordYear.SelectedValue == "Select Value")
        {
            //Hide Panels
            DisplayUniPanel.Visible = false;
            DisplayPanel.Visible = false;

        }
        else
        {
            //Display Panels, Find and Store Universities and Load All Criterias
            DisplayUniPanel.Visible = true;
            Functions.UpdateUniversityDataField(DropDownListUniversities, DropDownRecordYear);
            Functions.LoadCriteriaList(CheckBoxCriteria);
        }

    }


    protected void logOutButton_Click(object sender, EventArgs e)
    {
        //Kill Session
        Session.Abandon();

        //Redirect user to Login Page
        Response.Redirect("/Default.aspx");
    }

    protected void BtnSelect_Click(object sender, EventArgs e)
    {
        //If there is a value in the Universities Drop Down
        if (DropDownListUniversities.SelectedValue != "")
        {
            //Load Data From Table - Auto Disposable
            using (MSAAnalyserEntities context = new MSAAnalyserEntities())
            {
                //Show Panel
                DisplayPanel.Visible = true;

                //Find Unversity that belongs to a specific Table Record
                TableRecord tableRecord = context.TableRecords.FirstOrDefault(r => r.OrganisationName == DropDownListUniversities.SelectedItem.Text);

                //Sector CheckBoxes - Split String and Fill In CheckBoxes
                string[] sectorArray = new string[] { };
                sectorArray = tableRecord.Sector.ToString().Split(',');
                int sectorArrayLength = sectorArray.Length;
                for (int i = 0; i < sectorArrayLength - 1; i++)
                {
                    string checkItems = sectorArray[i];

                    for (int j = 0; j < CBServicesList.Items.Count - 1; j++)
                    {
                        if (CBServicesList.Items[j].ToString() == sectorArray[i])
                        {
                            CBServicesList.Items[j].Selected = true;
                            break;
                        }
                    }
                }

                //If the Collaberation Text Field is not Empty in the ADO.NET Model
                if (!string.IsNullOrEmpty(tableRecord.Collaboration))
                {
                    //Collaberation CheckBoxes Fill from Database
                    string[] collabArray = new string[] { };
                    collabArray = tableRecord.Collaboration.ToString().Split(',');
                    int collabArrayLength = collabArray.Length;
                    for (int i = 0; i < collabArrayLength - 1; i++)
                    {
                        string checkItems = collabArray[i];

                        for (int j = 0; j < CBCollabList.Items.Count - 1; j++)
                        {
                            if (CBCollabList.Items[j].ToString() == collabArray[i])
                            {
                                CBCollabList.Items[j].Selected = true;
                                break;
                            }
                        }
                    }
                }

                //Fill Data From Table DB
                DropDownSuspect.SelectedValue = tableRecord.SuspectedTemplate;

                //We Check if the Sector Other is not Null or Empty
                if (!string.IsNullOrEmpty(tableRecord.SectorOther))
                {
                    //Show Accociated Atributes
                    chkOther.Checked = true;
                    dvOtherTxt.Style["display"] = "block";
                    txtOther.Value = tableRecord.SectorOther;

                }
                else
                {
                    //Hide Accociated Atributes
                    chkOther.Checked = false;
                    dvOtherTxt.Style["display"] = "none";
                    txtOther.Value = "";
                }

                //Fill Data From DB - All Fields
                DropDownHQ.SelectedValue = tableRecord.HQ;
                DropDownSizeOfStatement.Text = tableRecord.SizeOfStatement;
                txtTurnOver.Text = Convert.ToString(tableRecord.TurnOver);
                txtURL.Text = tableRecord.URL;
                cbSigned.Checked = tableRecord.Signed.Value;
                DropDownSignedByOther.SelectedValue = tableRecord.SignedOther;
                cbHomePage.Checked = tableRecord.HomePage.Value;
                DropDownHomePageOther.SelectedValue = tableRecord.HomePageOther;
                cbIntroduction.Checked = tableRecord.Introduction.Value;
                txtCommentary.Value = tableRecord.Commentary;
                DropDownRecordYear.SelectedValue = tableRecord.RecordYear;

                //Find Criteria CheckBox Control
                CheckBoxList CriteriaCheckBox = (CheckBoxList)this.FindControl("CheckBoxCriteria");

                //Install Table Criteria
                TableCriteria tableCriteria;

                //For each Item thats in the Criteria CheckBoxes
                foreach (ListItem item in CriteriaCheckBox.Items)
                {
                    //Find Item in DB
                    tableCriteria = context.TableCriterias.FirstOrDefault(tc => tc.CriteriaName == item.Value);

                    //Select Record Criteria From the right Table Criterias
                    var tableRecordCriteria = context.TableRecordCriterias.Where(recordType => recordType.RecordId == tableRecord.Id && recordType.CriteriaId == tableCriteria.Id).FirstOrDefault();

                    //If it does not produce a null result..
                    if (tableRecordCriteria != null)
                    {
                        //Check if the Criteria is Reported or not
                        if (tableRecordCriteria.CriteriaReported == true)
                        {
                            //If it is then Select The Item 
                            item.Selected = tableRecordCriteria.CriteriaReported;

                        }
                        else
                        {
                            //Do not select the item
                            item.Selected = false;
                        }

                    }
                    else
                    {
                        //Do not select Any items
                        item.Selected = false;
                    }
                }
            }
        }
        else
        {
            //Display Alert Box
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('No Organisations exist for this Record Year!');", true);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        //Refresh Page On Cancel
        Response.Redirect(Request.RawUrl);
    }

    //Bind CheckBox Click and Hide Div Tag if False, show if true.
    protected string DefinirVisibilidad()
    {
        return this.chkOther.Checked == true ? "display:block" : "display:none";
    }

    //Update Button Click
    protected void btnUpdate_Click(object sender, EventArgs e)
    { 
        try
        {
            //Auto Disaposable Method
            using (MSAAnalyserEntities context = new MSAAnalyserEntities())
            {
                //Select Table Record from the Organisation Selected
                TableRecord tableRecords = context.TableRecords.FirstOrDefault(t => t.OrganisationName == DropDownListUniversities.SelectedItem.Text);

                //Check if any Sector CheckBoxes has been Selected
                bool hasAnyServicesBeenSelected = CBServicesList.Items.Cast<ListItem>().Any(i => i.Selected);

                //Check if any Criteria CheckBoxes has been Selected
                bool hasAnyCriteriasBeenSelected = CheckBoxCriteria.Items.Cast<ListItem>().Any(i => i.Selected);

                //Checking if any of the Criteria and Sector CheckBoxes has been selected - NOT
                if (!hasAnyServicesBeenSelected && !hasAnyCriteriasBeenSelected)
                {
                    FieldValidator.CssClass = "alert-danger";
                    FieldValidator.Visible = true;
                    FieldValidator.Text = "Sector and Criteria are all Required Fields!";

                }

                //Checking if any of the Criteria CheckBoxes has been selected - NOT
                else if (!hasAnyCriteriasBeenSelected)
                {
                    FieldValidator.CssClass = "alert-danger";
                    FieldValidator.Visible = true;
                    FieldValidator.Text = "You are required to at least select one Criteria!";

                }

                //Checking if any of the Sector CheckBoxes has been selected - NOT
                else if (!hasAnyServicesBeenSelected)
                {
                    FieldValidator.CssClass = "alert-danger";
                    FieldValidator.Visible = true;
                    FieldValidator.Text = "You are required to at least select one Sector!";

                } else
                {
                    //If all is good then store the Required Fields into the Entity Objects
                    tableRecords.OrganisationName = DropDownListUniversities.SelectedItem.Text;
                    tableRecords.HQ = DropDownHQ.SelectedValue;

                    //Finding the Sector CheckBoxes
                    CheckBoxList ServicesCheckBox = (CheckBoxList)this.FindControl("CBServicesList");

                    //Seeing if the Count is Greater then 0
                    if (ServicesCheckBox.Items.Count > 0)
                    {
                        //For each item thats selected, convert to List Item and Spilt with Comma and Store in Object
                        foreach (ListItem item in ServicesCheckBox.Items)
                        {
                            //If Item is checked
                            if (item.Selected)
                            {
                                tableRecords.Sector += item.Value + ",";
                            }
                        }
                    }

                    //If the checked Items are Greater then 0
                    if (CBCollabList.Items.Count > 0)
                    {
                        //For each Item that is selected Spilt with Comma and Store in Object
                        foreach (ListItem item in CBCollabList.Items)
                        {
                            if (item.Selected)
                            {
                                tableRecords.Collaboration += item.Value + ",";
                            }
                        }
                    }

                    //Store Required Fields into Entity Objects
                    tableRecords.SuspectedTemplate = DropDownSuspect.SelectedValue;

                    //If the Other CheckBox has been Checked, if true store in Object
                    if (chkOther.Checked == true && !string.IsNullOrEmpty(txtOther.Value))
                    {
                        tableRecords.SectorOther = txtOther.Value;

                    } else
                    {
                        //Hide Associated Objects
                        chkOther.Checked = false;
                        tableRecords.SectorOther = "";
                        txtOther.Value = "";
                    }

                    //Store Required Fields into Entity Objects
                    tableRecords.SizeOfStatement = DropDownSizeOfStatement.Text;
                    tableRecords.TurnOver = txtTurnOver.Text;
                    tableRecords.URL = txtURL.Text;
                    tableRecords.Signed = cbSigned.Checked;
                    tableRecords.SignedOther = DropDownSignedByOther.SelectedValue;
                    tableRecords.HomePage = cbHomePage.Checked;
                    tableRecords.HomePageOther = DropDownHomePageOther.SelectedValue;
                    tableRecords.Introduction = cbIntroduction.Checked;
                    tableRecords.Commentary = txtCommentary.Value;
                    tableRecords.RecordYear = DropDownRecordYear.SelectedValue;

                    //Installise New Table Record Criteria for Adding
                    TableRecordCriteria tableRecordCriteria = new TableRecordCriteria();

                    //Remove from TableRecordSubCriteria if Any
                    if (context.TableRecordSubCriterias.Where(trsc => trsc.RecordRecordId == tableRecords.Id) != null)
                    {
                        context.TableRecordSubCriterias.RemoveRange(context.TableRecordSubCriterias.Where(trsc => trsc.RecordRecordId == tableRecords.Id));
                    }

                    //Remove All current TableRecordCriteria Records
                    context.TableRecordCriterias.RemoveRange(context.TableRecordCriterias.Where(trc => trc.RecordId == tableRecords.Id));
                    
                    //Install New Table Criteria
                    TableCriteria tableCriteria;

                    //Find CheckBoxCriteria Control
                    CheckBoxList CriteriaCheckBox = (CheckBoxList)this.FindControl("CheckBoxCriteria");

                    //Check if the Selcted item count is greater then 0
                    if (CriteriaCheckBox.Items.Count > 0)
                    {
                        //For each item that is selected from the Criteria CheckBox Items
                        foreach (ListItem item in CriteriaCheckBox.Items)
                        {
                            //If the Item is Selected
                            if (item.Selected)
                            {
                                //Find and Select Each Value Corrosponding to the Item Value
                                tableCriteria = context.TableCriterias.FirstOrDefault(r => r.CriteriaName == item.Value);

                                //Attempt to Try Placing Records in appropriate Objects
                                try
                                {
                                    //Parse Fields into Objects to Fill Database
                                    tableRecordCriteria.RecordId = tableRecords.Id;
                                    tableRecordCriteria.CriteriaId = tableCriteria.Id;
                                    tableRecordCriteria.CriteriaReported = item.Selected;
                                    item.Selected = tableRecordCriteria.CriteriaReported;

                                    //Add Object Model to Database and Commit Changes
                                    context.TableRecordCriterias.Add(tableRecordCriteria);
                                    context.SaveChanges();

                                    //Display Success Message to User
                                    FieldValidator.CssClass = "alert-success";
                                    FieldValidator.Visible = true;
                                    FieldValidator.Text = DropDownListUniversities.SelectedItem.Text + " has successfully been updated!";

                                    //Refresh Page in 2.5 Seconds
                                    HtmlMeta meta = new HtmlMeta();
                                    meta.HttpEquiv = "Refresh";
                                    meta.Content = "2.5;url=Database-Edit.aspx";
                                    this.Page.Controls.Add(meta);

                                }
                                catch (Exception ex)
                                {
                                    //Throw Exception if anything was to go wrong
                                    Console.WriteLine(ex.Message);
                                }
                            }

                        }

                    }
                }

            }

        } catch (Exception ex)
        {
            //Throw Exception if anything was to go wrong
            Console.WriteLine(ex.Message);
        }
     }
   }
    

